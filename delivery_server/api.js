const express = require('express');
const router = express.Router();
const mysql = require('mysql');
const http = require('http');
const { Client } = require('pg');

const connection = new Client({
    connectionString: process.env.DATABASE_URL,
    ssl: true
});

// const connection = mysql.createConnection({
//     host: 'localhost',
//     user: 'root',
//     password: 'Mykel',
//     database: 'delivery'
// });

// connection.connect((err) => {
//     if (err) {
//         console.log(err.stack);
//         console.dir(err);
//         return;
//     }
//     console.log('DB connected successfully');
// });
connection.connect();

// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    response.data = [];
    res.status(501).json(response);
};

// Response handling
let response = {
    status: 200,
    data: [],
    message: null
};

router.post('/userAuth', (req, res) => {
    connection.query(`SELECT * FROM public.agent WHERE username = $1 AND password = $2`, [req.body.username, req.body.password], (err, result) => {
        if (err) {
            sendError(err, res);          
        } else {
            if (result.rows.length > 0) {
                response.status = 200;
                response.data = result.rows;
                response.message = 'Success';
            } else {
                response.status = 501;
                response.data = result.rows;
                response.message = 'Invalid username or password';
            }
            res.json(response);
            // console.log(result.rows);
        }
    });
});

router.post('/agentOrders', (req, res) => {
    connection.query(`
        SELECT id, image, product, total_price, TO_CHAR(order_date, 'DD Mon YYYY') AS order_date,
        agent_id, quantity, unit_price, payment_type, TO_CHAR(order_status_date, 'DD Mon YYYY') AS order_status_date,
        (SELECT name FROM customer WHERE id = orders.customer_id) AS customer_name,
        (SELECT address FROM customer WHERE id = orders.customer_id) AS address,
        (SELECT mobile FROM customer WHERE id = orders.customer_id) AS mobile
        FROM public.orders WHERE agent_id = $1 AND order_status = $2`, 
        [req.body.agentId, req.body.orderStatus], (error, results) => {
        if (error) {
            sendError(error, res);          
        } else {
            response.status = 200;
            response.data = results.rows;
            response.message = 'Success';
            res.json(response);
        }
    });
});

router.post('/updateOrder', (req, res) => {
    connection.query(`UPDATE orders SET order_status = $1, order_status_date = CURRENT_DATE WHERE id = $2`,
    [req.body.status, req.body.id], (error, results) => {
        if (error) {
            sendError(error, res);          
        } else {
            response.status = 200;
            response.data = results.rows;
            response.message = 'Success';
            res.json(response);
        }
    });
});

// connection.end();

module.exports = router;