--
-- PostgreSQL database dump
--

-- Dumped from database version 10.0
-- Dumped by pg_dump version 10.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: delivery; Type: SCHEMA; Schema: -; Owner: mykel
--

CREATE SCHEMA delivery;


ALTER SCHEMA delivery OWNER TO mykel;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = delivery, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: agent; Type: TABLE; Schema: delivery; Owner: mykel
--

CREATE TABLE agent (
    id bigint NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL
);


ALTER TABLE agent OWNER TO mykel;

--
-- Name: agent_id_seq; Type: SEQUENCE; Schema: delivery; Owner: mykel
--

CREATE SEQUENCE agent_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE agent_id_seq OWNER TO mykel;

--
-- Name: agent_id_seq; Type: SEQUENCE OWNED BY; Schema: delivery; Owner: mykel
--

ALTER SEQUENCE agent_id_seq OWNED BY agent.id;


--
-- Name: customer; Type: TABLE; Schema: delivery; Owner: mykel
--

CREATE TABLE customer (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    address character varying(500) NOT NULL,
    mobile character varying(255) NOT NULL
);


ALTER TABLE customer OWNER TO mykel;

--
-- Name: customer_id_seq; Type: SEQUENCE; Schema: delivery; Owner: mykel
--

CREATE SEQUENCE customer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_id_seq OWNER TO mykel;

--
-- Name: customer_id_seq; Type: SEQUENCE OWNED BY; Schema: delivery; Owner: mykel
--

ALTER SEQUENCE customer_id_seq OWNED BY customer.id;


--
-- Name: orders; Type: TABLE; Schema: delivery; Owner: mykel
--

CREATE TABLE orders (
    id bigint NOT NULL,
    agent_id bigint NOT NULL,
    customer_id bigint NOT NULL,
    order_id character varying(255) NOT NULL,
    product character varying(255) NOT NULL,
    quantity bigint NOT NULL,
    unit_price double precision NOT NULL,
    total_price double precision NOT NULL,
    order_date date NOT NULL,
    order_status character(1) NOT NULL,
    order_status_date date,
    payment_type character varying(255) NOT NULL,
    image character varying(255) NOT NULL
);


ALTER TABLE orders OWNER TO mykel;

--
-- Name: orders_id_seq; Type: SEQUENCE; Schema: delivery; Owner: mykel
--

CREATE SEQUENCE orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE orders_id_seq OWNER TO mykel;

--
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: delivery; Owner: mykel
--

ALTER SEQUENCE orders_id_seq OWNED BY orders.id;


--
-- Name: agent id; Type: DEFAULT; Schema: delivery; Owner: mykel
--

ALTER TABLE ONLY agent ALTER COLUMN id SET DEFAULT nextval('agent_id_seq'::regclass);


--
-- Name: customer id; Type: DEFAULT; Schema: delivery; Owner: mykel
--

ALTER TABLE ONLY customer ALTER COLUMN id SET DEFAULT nextval('customer_id_seq'::regclass);


--
-- Name: orders id; Type: DEFAULT; Schema: delivery; Owner: mykel
--

ALTER TABLE ONLY orders ALTER COLUMN id SET DEFAULT nextval('orders_id_seq'::regclass);


--
-- Data for Name: agent; Type: TABLE DATA; Schema: delivery; Owner: mykel
--

COPY agent (id, username, password) FROM stdin;
1	username	password
2	username1	password1
\.


--
-- Data for Name: customer; Type: TABLE DATA; Schema: delivery; Owner: mykel
--

COPY customer (id, name, address, mobile) FROM stdin;
1	Bimpe Ayoola	Alagomeji, Yaba, Lagos	07031801280
2	Oluwawumi Sowumi	No 58, Magodo, Lagos	07032801380
3	Rike Enomate	No 9C, Berger, Lagos	08012801380
4	Kunle Oladipo	No 9B, Mojisola Close, Off Mercy Eneli, Surulere, Lagos	07039484855
\.


--
-- Data for Name: orders; Type: TABLE DATA; Schema: delivery; Owner: mykel
--

COPY orders (id, agent_id, customer_id, order_id, product, quantity, unit_price, total_price, order_date, order_status, order_status_date, payment_type, image) FROM stdin;
1	1	1	92c952	accusamus beatae ad facilis cum similique qui sunt	2	2500	5000	2017-10-30	D	2017-10-23	Pay on Delivery	http://placehold.it/600/92c952
2	1	2	771796	reprehenderit est deserunt velit ipsam	1	500	500	2017-10-11	C	2017-10-23	Pay on Delivery	http://placehold.it/150/771796
3	1	3	24f355	officia porro iure quia iusto qui ipsa ut modi	1	3999.98999999999978	3999.98999999999978	2017-10-11	D	2017-10-23	Pay on Delivery	http://placehold.it/150/24f355
4	1	3	d32776	culpa odio esse rerum omnis laboriosam voluptate repudiandae	1	4999.98999999999978	4999.98999999999978	2017-10-03	D	2017-10-08	Pay on Delivery	http://placehold.it/150/d32776
5	1	4	f66b97	natus nisi omnis corporis facere molestiae rerum in	1	200999.989999999991	200999.989999999991	2017-09-03	C	2017-09-07	Pay on Delivery	http://placehold.it/150/f66b97
6	1	4	b0f7cc	officia delectus consequatur vero aut veniam explicabo molestias	3	30000	10000	2017-10-21	D	2017-10-24	Pay on Delivery	http://placehold.it/150/b0f7cc
7	1	2	810b14	beatae et provident et ut vel	1	11000	11000	2017-10-21	P	\N	Pay on Delivery	http://placehold.it/150/810b14
8	2	2	66b7d2	mollitia soluta ut rerum eos aliquam consequatur perspiciatis maiores	1	12000	12000	2017-10-21	P	\N	Pay on Delivery	http://placehold.it/150/66b7d2
\.


--
-- Name: agent_id_seq; Type: SEQUENCE SET; Schema: delivery; Owner: mykel
--

SELECT pg_catalog.setval('agent_id_seq', 2, true);


--
-- Name: customer_id_seq; Type: SEQUENCE SET; Schema: delivery; Owner: mykel
--

SELECT pg_catalog.setval('customer_id_seq', 4, true);


--
-- Name: orders_id_seq; Type: SEQUENCE SET; Schema: delivery; Owner: mykel
--

SELECT pg_catalog.setval('orders_id_seq', 8, true);


--
-- Name: idx_16398_unique_id; Type: INDEX; Schema: delivery; Owner: mykel
--

CREATE UNIQUE INDEX idx_16398_unique_id ON agent USING btree (id);


--
-- Name: idx_16407_unique_id; Type: INDEX; Schema: delivery; Owner: mykel
--

CREATE UNIQUE INDEX idx_16407_unique_id ON customer USING btree (id);


--
-- Name: idx_16416_unique_id; Type: INDEX; Schema: delivery; Owner: mykel
--

CREATE UNIQUE INDEX idx_16416_unique_id ON orders USING btree (id);


--
-- PostgreSQL database dump complete
--

