import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'dashboard',
	moduleId: module.id,
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.common.css']
})

export class DashboardComponent implements OnInit {

	constructor() { }

	ngOnInit() { }
}