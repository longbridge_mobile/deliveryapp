"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var dialogModule = require("ui/dialogs");
var router_1 = require("nativescript-angular/router");
var localStorage = require("nativescript-localstorage");
var login_service_service_1 = require("../services/login-service.service");
var HomeComponent = /** @class */ (function () {
    function HomeComponent(page, loginService, routerExt) {
        this.page = page;
        this.loginService = loginService;
        this.routerExt = routerExt;
        this.isPassword = true;
        this.username = 'username';
        this.password = 'password';
        this.isBusy = false;
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
    };
    HomeComponent.prototype.signIn = function () {
        return __awaiter(this, void 0, void 0, function () {
            var resp, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.busyToggle();
                        if (!localStorage.getItem('url')) {
                            alert('API end point is empty. Go to settings to set it');
                            this.busyToggle();
                            return [2 /*return*/, false];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 5, , 6]);
                        if (!(this.username !== '' && this.password !== '')) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.loginService.userAuth(this.username.toLowerCase(), this.password)];
                    case 2:
                        resp = _a.sent();
                        if (resp.status === 200) {
                            this.routerExt.navigate(['/dashboard'], { clearHistory: true });
                        }
                        else {
                            dialogModule.alert('Invalid username or password');
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        dialogModule.alert('Username and Password are required!');
                        _a.label = 4;
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        err_1 = _a.sent();
                        console.log(err_1);
                        return [3 /*break*/, 6];
                    case 6:
                        this.busyToggle();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeComponent.prototype.gotoSettings = function () {
        this.routerExt.navigate(['/settings']);
    };
    HomeComponent.prototype.passToggle = function () {
        this.isPassword = !this.isPassword;
    };
    HomeComponent.prototype.busyToggle = function () {
        this.isBusy = !this.isBusy;
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: "Home",
            moduleId: module.id,
            templateUrl: "./home.component.html",
            styleUrls: ["./home-common.css"]
        }),
        __metadata("design:paramtypes", [page_1.Page,
            login_service_service_1.LoginServiceService,
            router_1.RouterExtensions])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCxnQ0FBK0I7QUFDL0IseUNBQTJDO0FBQzNDLHNEQUErRDtBQUMvRCx3REFBMEQ7QUFFMUQsMkVBQXdFO0FBU3hFO0lBTUksdUJBQ1ksSUFBVSxFQUNWLFlBQWlDLEVBQ2pDLFNBQTJCO1FBRjNCLFNBQUksR0FBSixJQUFJLENBQU07UUFDVixpQkFBWSxHQUFaLFlBQVksQ0FBcUI7UUFDakMsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFSdkMsZUFBVSxHQUFZLElBQUksQ0FBQztRQUMzQixhQUFRLEdBQVcsVUFBVSxDQUFDO1FBQzlCLGFBQVEsR0FBVyxVQUFVLENBQUM7UUFDOUIsV0FBTSxHQUFZLEtBQUssQ0FBQztJQU1yQixDQUFDO0lBRUosZ0NBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztJQUNyQyxDQUFDO0lBRUssOEJBQU0sR0FBWjs7Ozs7O3dCQUNJLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQzt3QkFDbEIsRUFBRSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDL0IsS0FBSyxDQUFDLGtEQUFrRCxDQUFDLENBQUM7NEJBQzFELElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQzs0QkFDbEIsTUFBTSxnQkFBQyxLQUFLLEVBQUM7d0JBQ2pCLENBQUM7Ozs7NkJBR08sQ0FBQSxJQUFJLENBQUMsUUFBUSxLQUFLLEVBQUUsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLEVBQUUsQ0FBQSxFQUE1Qyx3QkFBNEM7d0JBQ2pDLHFCQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFBOzt3QkFBbkYsSUFBSSxHQUFHLFNBQTRFO3dCQUN2RixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUM7NEJBQ3RCLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUUsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDcEUsQ0FBQzt3QkFBQyxJQUFJLENBQUMsQ0FBQzs0QkFDSixZQUFZLENBQUMsS0FBSyxDQUFDLDhCQUE4QixDQUFDLENBQUM7d0JBQ3ZELENBQUM7Ozt3QkFFRCxZQUFZLENBQUMsS0FBSyxDQUFDLHFDQUFxQyxDQUFDLENBQUM7Ozs7O3dCQUk5RCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUcsQ0FBQyxDQUFDOzs7d0JBRXJCLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQzs7Ozs7S0FDckI7SUFFRCxvQ0FBWSxHQUFaO1FBQ0ksSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFRCxrQ0FBVSxHQUFWO1FBQ0ksSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDdkMsQ0FBQztJQUVELGtDQUFVLEdBQVY7UUFDSSxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUMvQixDQUFDO0lBcERRLGFBQWE7UUFQekIsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNO1lBQ2hCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsdUJBQXVCO1lBQ3BDLFNBQVMsRUFBRSxDQUFDLG1CQUFtQixDQUFDO1NBQ25DLENBQUM7eUNBU29CLFdBQUk7WUFDSSwyQ0FBbUI7WUFDdEIseUJBQWdCO09BVDlCLGFBQWEsQ0FxRHpCO0lBQUQsb0JBQUM7Q0FBQSxBQXJERCxJQXFEQztBQXJEWSxzQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidWkvcGFnZVwiO1xuaW1wb3J0ICogYXMgZGlhbG9nTW9kdWxlIGZyb20gJ3VpL2RpYWxvZ3MnO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgKiBhcyBsb2NhbFN0b3JhZ2UgZnJvbSAnbmF0aXZlc2NyaXB0LWxvY2Fsc3RvcmFnZSc7XG5cbmltcG9ydCB7IExvZ2luU2VydmljZVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9sb2dpbi1zZXJ2aWNlLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogXCJIb21lXCIsXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgICB0ZW1wbGF0ZVVybDogXCIuL2hvbWUuY29tcG9uZW50Lmh0bWxcIixcbiAgICBzdHlsZVVybHM6IFtcIi4vaG9tZS1jb21tb24uY3NzXCJdXG59KVxuXG5leHBvcnQgY2xhc3MgSG9tZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgaXNQYXNzd29yZDogYm9vbGVhbiA9IHRydWU7XG4gICAgdXNlcm5hbWU6IHN0cmluZyA9ICd1c2VybmFtZSc7XG4gICAgcGFzc3dvcmQ6IHN0cmluZyA9ICdwYXNzd29yZCc7XG4gICAgaXNCdXN5OiBib29sZWFuID0gZmFsc2U7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJpdmF0ZSBwYWdlOiBQYWdlLFxuICAgICAgICBwcml2YXRlIGxvZ2luU2VydmljZTogTG9naW5TZXJ2aWNlU2VydmljZSxcbiAgICAgICAgcHJpdmF0ZSByb3V0ZXJFeHQ6IFJvdXRlckV4dGVuc2lvbnNcbiAgICApIHt9XG5cbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XG4gICAgfVxuICAgIFxuICAgIGFzeW5jIHNpZ25JbigpIHtcbiAgICAgICAgdGhpcy5idXN5VG9nZ2xlKCk7XG4gICAgICAgIGlmICghbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VybCcpKSB7XG4gICAgICAgICAgICBhbGVydCgnQVBJIGVuZCBwb2ludCBpcyBlbXB0eS4gR28gdG8gc2V0dGluZ3MgdG8gc2V0IGl0Jyk7XG4gICAgICAgICAgICB0aGlzLmJ1c3lUb2dnbGUoKTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBpZiAodGhpcy51c2VybmFtZSAhPT0gJycgJiYgdGhpcy5wYXNzd29yZCAhPT0gJycpIHtcbiAgICAgICAgICAgICAgICBsZXQgcmVzcCA9IGF3YWl0IHRoaXMubG9naW5TZXJ2aWNlLnVzZXJBdXRoKHRoaXMudXNlcm5hbWUudG9Mb3dlckNhc2UoKSwgdGhpcy5wYXNzd29yZCk7XG4gICAgICAgICAgICAgICAgaWYgKHJlc3Auc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXJFeHQubmF2aWdhdGUoWycvZGFzaGJvYXJkJ10sIHsgY2xlYXJIaXN0b3J5OiB0cnVlIH0pO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGRpYWxvZ01vZHVsZS5hbGVydCgnSW52YWxpZCB1c2VybmFtZSBvciBwYXNzd29yZCcpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgZGlhbG9nTW9kdWxlLmFsZXJ0KCdVc2VybmFtZSBhbmQgUGFzc3dvcmQgYXJlIHJlcXVpcmVkIScpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycik7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5idXN5VG9nZ2xlKCk7XG4gICAgfVxuXG4gICAgZ290b1NldHRpbmdzKCk6IHZvaWQge1xuICAgICAgICB0aGlzLnJvdXRlckV4dC5uYXZpZ2F0ZShbJy9zZXR0aW5ncyddKTtcbiAgICB9XG5cbiAgICBwYXNzVG9nZ2xlKCkge1xuICAgICAgICB0aGlzLmlzUGFzc3dvcmQgPSAhdGhpcy5pc1Bhc3N3b3JkO1xuICAgIH1cblxuICAgIGJ1c3lUb2dnbGUoKSB7XG4gICAgICAgIHRoaXMuaXNCdXN5ID0gIXRoaXMuaXNCdXN5O1xuICAgIH1cbn1cbiJdfQ==