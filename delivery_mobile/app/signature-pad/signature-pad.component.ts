import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ModalDialogParams } from 'nativescript-angular/modal-dialog';
import { Page } from 'ui/page';

@Component({
	selector: 'signature-pad',
	moduleId: module.id,
	templateUrl: './signature-pad.component.html',
	styleUrls: ['./signature-pad.common.css']
})

export class SignaturePadComponent implements OnInit {
	@ViewChild('DrawingPad') drawingPad: ElementRef;

	constructor(
		private params: ModalDialogParams,
		private page: Page
	) {
		this.page.on('unloaded', () => {
			this.params.closeCallback('No Data');
		});
	}

	ngOnInit() { }

	getMyDrawing(): void {
		let pad = this.drawingPad.nativeElement;
		pad.getDrawing().then((data) => {
			this.params.closeCallback(data);
		})
		.catch(err => console.log(err));
	}

	clearMyDrawing(): void {
		let pad = this.drawingPad.nativeElement;
		pad.clearDrawing();
	}
}