"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var localStorage = require("nativescript-localstorage");
var dialogModule = require("ui/dialogs");
var router_1 = require("nativescript-angular/router");
var SettingsComponent = /** @class */ (function () {
    function SettingsComponent(routerExt) {
        this.routerExt = routerExt;
        this.urlString = '';
    }
    SettingsComponent.prototype.ngOnInit = function () {
        this.urlString = localStorage.getItem('url');
        if (!this.urlString) {
            this.urlString = 'e.g. http://path-to-api:port/api';
        }
    };
    SettingsComponent.prototype.setURL = function () {
        var _this = this;
        dialogModule.prompt('Enter URL').then(function (val) {
            _this.urlString = val.text;
            localStorage.setItem('url', val.text);
        })
            .catch(function (err) { return console.log(err); });
    };
    SettingsComponent.prototype.goBack = function () {
        this.routerExt.back();
    };
    SettingsComponent = __decorate([
        core_1.Component({
            selector: 'settings',
            moduleId: module.id,
            templateUrl: './settings.component.html',
            styleUrls: ['./settings.component.css']
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions])
    ], SettingsComponent);
    return SettingsComponent;
}());
exports.SettingsComponent = SettingsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3MuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2V0dGluZ3MuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELHdEQUEwRDtBQUMxRCx5Q0FBMkM7QUFDM0Msc0RBQStEO0FBUy9EO0lBR0MsMkJBQW9CLFNBQTJCO1FBQTNCLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBRi9DLGNBQVMsR0FBVyxFQUFFLENBQUM7SUFFNEIsQ0FBQztJQUVwRCxvQ0FBUSxHQUFSO1FBQ0MsSUFBSSxDQUFDLFNBQVMsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDckIsSUFBSSxDQUFDLFNBQVMsR0FBRyxrQ0FBa0MsQ0FBQztRQUNyRCxDQUFDO0lBQ0YsQ0FBQztJQUVELGtDQUFNLEdBQU47UUFBQSxpQkFNQztRQUxBLFlBQVksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsR0FBRztZQUN6QyxLQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUM7WUFDMUIsWUFBWSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3ZDLENBQUMsQ0FBQzthQUNELEtBQUssQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQWhCLENBQWdCLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBRUQsa0NBQU0sR0FBTjtRQUNDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQXRCVyxpQkFBaUI7UUFQN0IsZ0JBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxVQUFVO1lBQ3BCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsMkJBQTJCO1lBQ3hDLFNBQVMsRUFBRSxDQUFDLDBCQUEwQixDQUFDO1NBQ3ZDLENBQUM7eUNBSzhCLHlCQUFnQjtPQUhuQyxpQkFBaUIsQ0F1QjdCO0lBQUQsd0JBQUM7Q0FBQSxBQXZCRCxJQXVCQztBQXZCWSw4Q0FBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0ICogYXMgbG9jYWxTdG9yYWdlIGZyb20gJ25hdGl2ZXNjcmlwdC1sb2NhbHN0b3JhZ2UnO1xuaW1wb3J0ICogYXMgZGlhbG9nTW9kdWxlIGZyb20gJ3VpL2RpYWxvZ3MnO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlcic7XG5cbkBDb21wb25lbnQoe1xuXHRzZWxlY3RvcjogJ3NldHRpbmdzJyxcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcblx0dGVtcGxhdGVVcmw6ICcuL3NldHRpbmdzLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vc2V0dGluZ3MuY29tcG9uZW50LmNzcyddXG59KVxuXG5leHBvcnQgY2xhc3MgU2V0dGluZ3NDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXHR1cmxTdHJpbmc6IHN0cmluZyA9ICcnO1xuXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyRXh0OiBSb3V0ZXJFeHRlbnNpb25zKSB7IH1cblxuXHRuZ09uSW5pdCgpIHtcblx0XHR0aGlzLnVybFN0cmluZyA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1cmwnKTtcblx0XHRpZiAoIXRoaXMudXJsU3RyaW5nKSB7XG5cdFx0XHR0aGlzLnVybFN0cmluZyA9ICdlLmcuIGh0dHA6Ly9wYXRoLXRvLWFwaTpwb3J0L2FwaSc7XG5cdFx0fVxuXHR9XG5cblx0c2V0VVJMKCk6IHZvaWQge1xuXHRcdGRpYWxvZ01vZHVsZS5wcm9tcHQoJ0VudGVyIFVSTCcpLnRoZW4oKHZhbCkgPT4ge1xuXHRcdFx0dGhpcy51cmxTdHJpbmcgPSB2YWwudGV4dDtcblx0XHRcdGxvY2FsU3RvcmFnZS5zZXRJdGVtKCd1cmwnLCB2YWwudGV4dCk7XG5cdFx0fSlcblx0XHQuY2F0Y2goZXJyID0+IGNvbnNvbGUubG9nKGVycikpO1xuXHR9XG5cblx0Z29CYWNrKCk6IHZvaWQge1xuXHRcdHRoaXMucm91dGVyRXh0LmJhY2soKTtcblx0fVxufSJdfQ==