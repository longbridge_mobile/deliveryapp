"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var localStorage = require("nativescript-localstorage");
require("rxjs/add/operator/toPromise");
var LoginServiceService = /** @class */ (function () {
    function LoginServiceService(http) {
        this.http = http;
        this.url = '';
        this.url = localStorage.getItem('url');
    }
    LoginServiceService.prototype.userAuth = function (username, password) {
        return __awaiter(this, void 0, void 0, function () {
            var header, body, response, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.url = localStorage.getItem('url');
                        console.log(this.url);
                        header = new http_1.Headers();
                        header.append('Content-Type', 'application/json');
                        body = {
                            username: username,
                            password: 'cGFzc3dvcmQ='
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.http.post(this.url + "/userAuth", JSON.stringify(body), { headers: header }).toPromise()];
                    case 2:
                        response = _a.sent();
                        return [2 /*return*/, response.json()];
                    case 3:
                        err_1 = _a.sent();
                        console.dir(err_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    LoginServiceService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], LoginServiceService);
    return LoginServiceService;
}());
exports.LoginServiceService = LoginServiceService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tc2VydmljZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4tc2VydmljZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBQzNDLHNDQUE4QztBQUM5Qyx3REFBMEQ7QUFDMUQsdUNBQXFDO0FBR3JDO0lBR0ksNkJBQW9CLElBQVU7UUFBVixTQUFJLEdBQUosSUFBSSxDQUFNO1FBRjlCLFFBQUcsR0FBVyxFQUFFLENBQUM7UUFHYixJQUFJLENBQUMsR0FBRyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVLLHNDQUFRLEdBQWQsVUFBZSxRQUFnQixFQUFFLFFBQWdCOzs7Ozs7d0JBQzdDLElBQUksQ0FBQyxHQUFHLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDdkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQ2xCLE1BQU0sR0FBRyxJQUFJLGNBQU8sRUFBRSxDQUFDO3dCQUMzQixNQUFNLENBQUMsTUFBTSxDQUFDLGNBQWMsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO3dCQUU1QyxJQUFJLEdBQUc7NEJBQ1QsUUFBUSxFQUFFLFFBQVE7NEJBQ2xCLFFBQVEsRUFBRSxjQUFjO3lCQUMzQixDQUFDOzs7O3dCQUdpQixxQkFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBSSxJQUFJLENBQUMsR0FBRyxjQUFXLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRSxFQUFBOzt3QkFBNUcsUUFBUSxHQUFHLFNBQWlHO3dCQUNoSCxzQkFBTyxRQUFRLENBQUMsSUFBSSxFQUFFLEVBQUM7Ozt3QkFHdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFHLENBQUMsQ0FBQzs7Ozs7O0tBRXhCO0lBekJRLG1CQUFtQjtRQUQvQixpQkFBVSxFQUFFO3lDQUlpQixXQUFJO09BSHJCLG1CQUFtQixDQTJCL0I7SUFBRCwwQkFBQztDQUFBLEFBM0JELElBMkJDO0FBM0JZLGtEQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHAsIEhlYWRlcnMgfSBmcm9tICdAYW5ndWxhci9odHRwJztcbmltcG9ydCAqIGFzIGxvY2FsU3RvcmFnZSBmcm9tICduYXRpdmVzY3JpcHQtbG9jYWxzdG9yYWdlJztcbmltcG9ydCAncnhqcy9hZGQvb3BlcmF0b3IvdG9Qcm9taXNlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIExvZ2luU2VydmljZVNlcnZpY2Uge1xuICAgIHVybDogc3RyaW5nID0gJyc7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHApIHtcbiAgICAgICAgdGhpcy51cmwgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXJsJyk7XG4gICAgfVxuXG4gICAgYXN5bmMgdXNlckF1dGgodXNlcm5hbWU6IHN0cmluZywgcGFzc3dvcmQ6IHN0cmluZykge1xuICAgICAgICB0aGlzLnVybCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1cmwnKTtcbiAgICAgICAgY29uc29sZS5sb2codGhpcy51cmwpO1xuICAgICAgICBsZXQgaGVhZGVyID0gbmV3IEhlYWRlcnMoKTtcbiAgICAgICAgaGVhZGVyLmFwcGVuZCgnQ29udGVudC1UeXBlJywgJ2FwcGxpY2F0aW9uL2pzb24nKTtcblxuICAgICAgICBjb25zdCBib2R5ID0ge1xuICAgICAgICAgICAgdXNlcm5hbWU6IHVzZXJuYW1lLFxuICAgICAgICAgICAgcGFzc3dvcmQ6ICdjR0Z6YzNkdmNtUT0nXG4gICAgICAgIH07XG5cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGxldCByZXNwb25zZSA9IGF3YWl0IHRoaXMuaHR0cC5wb3N0KGAke3RoaXMudXJsfS91c2VyQXV0aGAsIEpTT04uc3RyaW5naWZ5KGJvZHkpLCB7aGVhZGVyczogaGVhZGVyfSkudG9Qcm9taXNlKCk7XG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2UuanNvbigpO1xuICAgICAgICB9XG4gICAgICAgIGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIGNvbnNvbGUuZGlyKGVycik7XG4gICAgICAgIH1cbiAgICB9XG5cbn0iXX0=