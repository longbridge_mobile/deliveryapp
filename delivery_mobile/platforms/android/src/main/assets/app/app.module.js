"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var element_registry_1 = require("nativescript-angular/element-registry");
element_registry_1.registerElement('DrawingPad', function () { return require('nativescript-drawingpad').DrawingPad; });
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var router_1 = require("nativescript-angular/router");
var router_2 = require("nativescript-angular/router");
var http_1 = require("nativescript-angular/http");
var forms_1 = require("nativescript-angular/forms");
// import { AppRoutingModule } from "./app-routing.module";
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
var login_service_service_1 = require("./services/login-service.service");
var order_service_1 = require("./services/order.service");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            bootstrap: [
                app_component_1.AppComponent
            ],
            imports: [
                nativescript_module_1.NativeScriptModule,
                router_2.NativeScriptRouterModule,
                router_2.NativeScriptRouterModule.forRoot(app_routing_module_1.routes),
                http_1.NativeScriptHttpModule,
                forms_1.NativeScriptFormsModule
                // AppRoutingModule
            ],
            declarations: [
                app_component_1.AppComponent
            ].concat(app_routing_module_1.navcomponents),
            providers: [
                { provide: core_1.NgModuleFactoryLoader, useClass: router_1.NSModuleFactoryLoader },
                login_service_service_1.LoginServiceService,
                order_service_1.OrderService
            ],
            schemas: [
                core_1.NO_ERRORS_SCHEMA
            ]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSwwRUFBd0U7QUFDeEUsa0NBQWUsQ0FBQyxZQUFZLEVBQUUsY0FBTSxPQUFBLE9BQU8sQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLFVBQVUsRUFBN0MsQ0FBNkMsQ0FBQyxDQUFDO0FBRW5GLHNDQUFrRjtBQUNsRixnRkFBOEU7QUFDOUUsc0RBQW9FO0FBQ3BFLHNEQUF1RTtBQUN2RSxrREFBbUU7QUFDbkUsb0RBQXFFO0FBRXJFLDJEQUEyRDtBQUMzRCwyREFBNkQ7QUFDN0QsaURBQStDO0FBRS9DLDBFQUF1RTtBQUN2RSwwREFBd0Q7QUEyQnhEO0lBQUE7SUFBeUIsQ0FBQztJQUFiLFNBQVM7UUF6QnJCLGVBQVEsQ0FBQztZQUNOLFNBQVMsRUFBRTtnQkFDUCw0QkFBWTthQUNmO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLHdDQUFrQjtnQkFDbEIsaUNBQXdCO2dCQUN4QixpQ0FBd0IsQ0FBQyxPQUFPLENBQUMsMkJBQU0sQ0FBQztnQkFDeEMsNkJBQXNCO2dCQUN0QiwrQkFBdUI7Z0JBQ3ZCLG1CQUFtQjthQUN0QjtZQUNELFlBQVk7Z0JBQ1IsNEJBQVk7cUJBQ1Qsa0NBQWEsQ0FDbkI7WUFDRCxTQUFTLEVBQUU7Z0JBQ1AsRUFBRSxPQUFPLEVBQUUsNEJBQXFCLEVBQUUsUUFBUSxFQUFFLDhCQUFxQixFQUFFO2dCQUNuRSwyQ0FBbUI7Z0JBQ25CLDRCQUFZO2FBQ2Y7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsdUJBQWdCO2FBQ25CO1NBQ0osQ0FBQztPQUNXLFNBQVMsQ0FBSTtJQUFELGdCQUFDO0NBQUEsQUFBMUIsSUFBMEI7QUFBYiw4QkFBUyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IHJlZ2lzdGVyRWxlbWVudCB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL2VsZW1lbnQtcmVnaXN0cnknO1xucmVnaXN0ZXJFbGVtZW50KCdEcmF3aW5nUGFkJywgKCkgPT4gcmVxdWlyZSgnbmF0aXZlc2NyaXB0LWRyYXdpbmdwYWQnKS5EcmF3aW5nUGFkKTtcblxuaW1wb3J0IHsgTmdNb2R1bGUsIE5nTW9kdWxlRmFjdG9yeUxvYWRlciwgTk9fRVJST1JTX1NDSEVNQSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvbmF0aXZlc2NyaXB0Lm1vZHVsZVwiO1xuaW1wb3J0IHsgTlNNb2R1bGVGYWN0b3J5TG9hZGVyIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHsgTmF0aXZlU2NyaXB0SHR0cE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9odHRwXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRGb3Jtc01vZHVsZSB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL2Zvcm1zJztcblxuLy8gaW1wb3J0IHsgQXBwUm91dGluZ01vZHVsZSB9IGZyb20gXCIuL2FwcC1yb3V0aW5nLm1vZHVsZVwiO1xuaW1wb3J0IHsgcm91dGVzLCBuYXZjb21wb25lbnRzIH0gZnJvbSBcIi4vYXBwLXJvdXRpbmcubW9kdWxlXCI7XG5pbXBvcnQgeyBBcHBDb21wb25lbnQgfSBmcm9tIFwiLi9hcHAuY29tcG9uZW50XCI7XG5cbmltcG9ydCB7IExvZ2luU2VydmljZVNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2xvZ2luLXNlcnZpY2Uuc2VydmljZSc7XG5pbXBvcnQgeyBPcmRlclNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL29yZGVyLnNlcnZpY2UnO1xuXG5ATmdNb2R1bGUoe1xuICAgIGJvb3RzdHJhcDogW1xuICAgICAgICBBcHBDb21wb25lbnRcbiAgICBdLFxuICAgIGltcG9ydHM6IFtcbiAgICAgICAgTmF0aXZlU2NyaXB0TW9kdWxlLFxuICAgICAgICBOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUsXG4gICAgICAgIE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZS5mb3JSb290KHJvdXRlcyksXG4gICAgICAgIE5hdGl2ZVNjcmlwdEh0dHBNb2R1bGUsIFxuICAgICAgICBOYXRpdmVTY3JpcHRGb3Jtc01vZHVsZVxuICAgICAgICAvLyBBcHBSb3V0aW5nTW9kdWxlXG4gICAgXSxcbiAgICBkZWNsYXJhdGlvbnM6IFtcbiAgICAgICAgQXBwQ29tcG9uZW50LFxuICAgICAgICAuLi5uYXZjb21wb25lbnRzXG4gICAgXSxcbiAgICBwcm92aWRlcnM6IFtcbiAgICAgICAgeyBwcm92aWRlOiBOZ01vZHVsZUZhY3RvcnlMb2FkZXIsIHVzZUNsYXNzOiBOU01vZHVsZUZhY3RvcnlMb2FkZXIgfSxcbiAgICAgICAgTG9naW5TZXJ2aWNlU2VydmljZSxcbiAgICAgICAgT3JkZXJTZXJ2aWNlXG4gICAgXSxcbiAgICBzY2hlbWFzOiBbXG4gICAgICAgIE5PX0VSUk9SU19TQ0hFTUFcbiAgICBdXG59KVxuZXhwb3J0IGNsYXNzIEFwcE1vZHVsZSB7IH1cbiJdfQ==