"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var localStorage = require("nativescript-localstorage");
require("rxjs/add/operator/toPromise");
var OrderService = /** @class */ (function () {
    function OrderService(http) {
        this.http = http;
        this.url = '';
        this.url = localStorage.getItem('url');
    }
    OrderService.prototype.userAuth = function (body, methodName) {
        return __awaiter(this, void 0, void 0, function () {
            var header, response, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.url = localStorage.getItem('url');
                        console.log(this.url);
                        if (!this.url) {
                            alert('API end point is empty. Go to settings to set it');
                        }
                        header = new http_1.Headers();
                        header.append('Content-Type', 'application/json');
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.http.post(this.url + "/" + methodName, JSON.stringify(body), { headers: header }).toPromise()];
                    case 2:
                        response = _a.sent();
                        return [2 /*return*/, response.json()];
                    case 3:
                        err_1 = _a.sent();
                        console.dir(err_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    OrderService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], OrderService);
    return OrderService;
}());
exports.OrderService = OrderService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3JkZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm9yZGVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0Msc0NBQThDO0FBQzlDLHdEQUEwRDtBQUMxRCx1Q0FBcUM7QUFHckM7SUFJSSxzQkFBb0IsSUFBVTtRQUFWLFNBQUksR0FBSixJQUFJLENBQU07UUFGOUIsUUFBRyxHQUFXLEVBQUUsQ0FBQztRQUdiLElBQUksQ0FBQyxHQUFHLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRUssK0JBQVEsR0FBZCxVQUFlLElBQUksRUFBRSxVQUFVOzs7Ozs7d0JBQzNCLElBQUksQ0FBQyxHQUFHLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDdkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQ3RCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7NEJBQ1osS0FBSyxDQUFDLGtEQUFrRCxDQUFDLENBQUM7d0JBQzlELENBQUM7d0JBQ0csTUFBTSxHQUFHLElBQUksY0FBTyxFQUFFLENBQUM7d0JBQzNCLE1BQU0sQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLGtCQUFrQixDQUFDLENBQUM7Ozs7d0JBRy9CLHFCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFJLElBQUksQ0FBQyxHQUFHLFNBQUksVUFBWSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBQTs7d0JBQW5ILFFBQVEsR0FBRyxTQUF3Rzt3QkFDdkgsc0JBQU8sUUFBUSxDQUFDLElBQUksRUFBRSxFQUFDOzs7d0JBR3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBRyxDQUFDLENBQUM7Ozs7OztLQUV4QjtJQXhCUSxZQUFZO1FBRHhCLGlCQUFVLEVBQUU7eUNBS2lCLFdBQUk7T0FKckIsWUFBWSxDQTBCeEI7SUFBRCxtQkFBQztDQUFBLEFBMUJELElBMEJDO0FBMUJZLG9DQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cCwgSGVhZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xuaW1wb3J0ICogYXMgbG9jYWxTdG9yYWdlIGZyb20gJ25hdGl2ZXNjcmlwdC1sb2NhbHN0b3JhZ2UnO1xuaW1wb3J0ICdyeGpzL2FkZC9vcGVyYXRvci90b1Byb21pc2UnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgT3JkZXJTZXJ2aWNlIHtcblxuICAgIHVybDogc3RyaW5nID0gJyc7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHApIHtcbiAgICAgICAgdGhpcy51cmwgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXJsJyk7XG4gICAgfVxuXG4gICAgYXN5bmMgdXNlckF1dGgoYm9keSwgbWV0aG9kTmFtZSkge1xuICAgICAgICB0aGlzLnVybCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1cmwnKTtcbiAgICAgICAgY29uc29sZS5sb2codGhpcy51cmwpO1xuICAgICAgICBpZiAoIXRoaXMudXJsKSB7XG4gICAgICAgICAgICBhbGVydCgnQVBJIGVuZCBwb2ludCBpcyBlbXB0eS4gR28gdG8gc2V0dGluZ3MgdG8gc2V0IGl0Jyk7XG4gICAgICAgIH1cbiAgICAgICAgbGV0IGhlYWRlciA9IG5ldyBIZWFkZXJzKCk7XG4gICAgICAgIGhlYWRlci5hcHBlbmQoJ0NvbnRlbnQtVHlwZScsICdhcHBsaWNhdGlvbi9qc29uJyk7XG5cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGxldCByZXNwb25zZSA9IGF3YWl0IHRoaXMuaHR0cC5wb3N0KGAke3RoaXMudXJsfS8ke21ldGhvZE5hbWV9YCwgSlNPTi5zdHJpbmdpZnkoYm9keSksIHsgaGVhZGVyczogaGVhZGVyIH0pLnRvUHJvbWlzZSgpO1xuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmpzb24oKTtcbiAgICAgICAgfVxuICAgICAgICBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICBjb25zb2xlLmRpcihlcnIpO1xuICAgICAgICB9XG4gICAgfVxuXG59Il19