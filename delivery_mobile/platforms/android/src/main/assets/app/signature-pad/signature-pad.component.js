"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var page_1 = require("ui/page");
var SignaturePadComponent = /** @class */ (function () {
    function SignaturePadComponent(params, page) {
        var _this = this;
        this.params = params;
        this.page = page;
        this.page.on('unloaded', function () {
            _this.params.closeCallback('No Data');
        });
    }
    SignaturePadComponent.prototype.ngOnInit = function () { };
    SignaturePadComponent.prototype.getMyDrawing = function () {
        var _this = this;
        var pad = this.drawingPad.nativeElement;
        pad.getDrawing().then(function (data) {
            _this.params.closeCallback(data);
        })
            .catch(function (err) { return console.log(err); });
    };
    SignaturePadComponent.prototype.clearMyDrawing = function () {
        var pad = this.drawingPad.nativeElement;
        pad.clearDrawing();
    };
    __decorate([
        core_1.ViewChild('DrawingPad'),
        __metadata("design:type", core_1.ElementRef)
    ], SignaturePadComponent.prototype, "drawingPad", void 0);
    SignaturePadComponent = __decorate([
        core_1.Component({
            selector: 'signature-pad',
            moduleId: module.id,
            templateUrl: './signature-pad.component.html',
            styleUrls: ['./signature-pad.common.css']
        }),
        __metadata("design:paramtypes", [modal_dialog_1.ModalDialogParams,
            page_1.Page])
    ], SignaturePadComponent);
    return SignaturePadComponent;
}());
exports.SignaturePadComponent = SignaturePadComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lnbmF0dXJlLXBhZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzaWduYXR1cmUtcGFkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF5RTtBQUN6RSxrRUFBc0U7QUFDdEUsZ0NBQStCO0FBUy9CO0lBR0MsK0JBQ1MsTUFBeUIsRUFDekIsSUFBVTtRQUZuQixpQkFPQztRQU5RLFdBQU0sR0FBTixNQUFNLENBQW1CO1FBQ3pCLFNBQUksR0FBSixJQUFJLENBQU07UUFFbEIsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxFQUFFO1lBQ3hCLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3RDLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUVELHdDQUFRLEdBQVIsY0FBYSxDQUFDO0lBRWQsNENBQVksR0FBWjtRQUFBLGlCQU1DO1FBTEEsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUM7UUFDeEMsR0FBRyxDQUFDLFVBQVUsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLElBQUk7WUFDMUIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDakMsQ0FBQyxDQUFDO2FBQ0QsS0FBSyxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBaEIsQ0FBZ0IsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFRCw4Q0FBYyxHQUFkO1FBQ0MsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUM7UUFDeEMsR0FBRyxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3BCLENBQUM7SUF4QndCO1FBQXhCLGdCQUFTLENBQUMsWUFBWSxDQUFDO2tDQUFhLGlCQUFVOzZEQUFDO0lBRHBDLHFCQUFxQjtRQVBqQyxnQkFBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLGVBQWU7WUFDekIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSxnQ0FBZ0M7WUFDN0MsU0FBUyxFQUFFLENBQUMsNEJBQTRCLENBQUM7U0FDekMsQ0FBQzt5Q0FNZ0IsZ0NBQWlCO1lBQ25CLFdBQUk7T0FMUCxxQkFBcUIsQ0EwQmpDO0lBQUQsNEJBQUM7Q0FBQSxBQTFCRCxJQTBCQztBQTFCWSxzREFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkLCBFbGVtZW50UmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNb2RhbERpYWxvZ1BhcmFtcyB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL21vZGFsLWRpYWxvZyc7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSAndWkvcGFnZSc7XG5cbkBDb21wb25lbnQoe1xuXHRzZWxlY3RvcjogJ3NpZ25hdHVyZS1wYWQnLFxuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxuXHR0ZW1wbGF0ZVVybDogJy4vc2lnbmF0dXJlLXBhZC5jb21wb25lbnQuaHRtbCcsXG5cdHN0eWxlVXJsczogWycuL3NpZ25hdHVyZS1wYWQuY29tbW9uLmNzcyddXG59KVxuXG5leHBvcnQgY2xhc3MgU2lnbmF0dXJlUGFkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblx0QFZpZXdDaGlsZCgnRHJhd2luZ1BhZCcpIGRyYXdpbmdQYWQ6IEVsZW1lbnRSZWY7XG5cblx0Y29uc3RydWN0b3IoXG5cdFx0cHJpdmF0ZSBwYXJhbXM6IE1vZGFsRGlhbG9nUGFyYW1zLFxuXHRcdHByaXZhdGUgcGFnZTogUGFnZVxuXHQpIHtcblx0XHR0aGlzLnBhZ2Uub24oJ3VubG9hZGVkJywgKCkgPT4ge1xuXHRcdFx0dGhpcy5wYXJhbXMuY2xvc2VDYWxsYmFjaygnTm8gRGF0YScpO1xuXHRcdH0pO1xuXHR9XG5cblx0bmdPbkluaXQoKSB7IH1cblxuXHRnZXRNeURyYXdpbmcoKTogdm9pZCB7XG5cdFx0bGV0IHBhZCA9IHRoaXMuZHJhd2luZ1BhZC5uYXRpdmVFbGVtZW50O1xuXHRcdHBhZC5nZXREcmF3aW5nKCkudGhlbigoZGF0YSkgPT4ge1xuXHRcdFx0dGhpcy5wYXJhbXMuY2xvc2VDYWxsYmFjayhkYXRhKTtcblx0XHR9KVxuXHRcdC5jYXRjaChlcnIgPT4gY29uc29sZS5sb2coZXJyKSk7XG5cdH1cblxuXHRjbGVhck15RHJhd2luZygpOiB2b2lkIHtcblx0XHRsZXQgcGFkID0gdGhpcy5kcmF3aW5nUGFkLm5hdGl2ZUVsZW1lbnQ7XG5cdFx0cGFkLmNsZWFyRHJhd2luZygpO1xuXHR9XG59Il19