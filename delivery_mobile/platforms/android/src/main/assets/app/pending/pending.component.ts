import { Component, OnInit } from '@angular/core';
import { Page } from 'ui/page';
import { OrderService } from '../services/order.service';
import { RouterExtensions } from 'nativescript-angular/router';
import { NavigationExtras } from '@angular/router';

@Component({
	selector: 'pending',
	moduleId: module.id,
	templateUrl: './pending.component.html',
	styleUrls: ['./pending.common.css']
})

export class PendingComponent implements OnInit {
	message: string = '';
	source: Array<any> = [];
	isBusy: boolean = false;

	constructor(
		private orderService: OrderService,
		private routerExt: RouterExtensions
	) { }

	async ngOnInit() {
		await this.getAgentOrders();
	}

	onItemTap(data) {
		let navigationExtras: NavigationExtras = {
			queryParams: {
				"data": JSON.stringify(this.source[data.index]),
				"status": "P",
				"statusText": "Pending"
			}
		};

		this.routerExt.navigate(['/order-details'], navigationExtras);
	}

	async getAgentOrders() {
		this.busyToggle();
		this.message = '';
		let body = {
			agentId: 1,
			orderStatus: 'P'
		};

		try {
			let response = await this.orderService.userAuth(body, 'agentOrders');
			if (response.status === 200) {
				this.source = response.data
			} else {
				this.message = 'You have no pending orders';
			}
		}
		catch (err) {
			console.log(err);
		}
		this.busyToggle();
	}

	busyToggle() {
		this.isBusy = !this.isBusy;
	}
}