"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var localStorage = require("nativescript-localstorage");
require("rxjs/add/operator/toPromise");
var LoginServiceService = /** @class */ (function () {
    function LoginServiceService(http) {
        this.http = http;
        this.url = '';
        this.url = localStorage.getItem('url');
    }
    LoginServiceService.prototype.userAuth = function (username, password) {
        return __awaiter(this, void 0, void 0, function () {
            var header, body, response, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.url = localStorage.getItem('url');
                        console.log(this.url);
                        header = new http_1.Headers();
                        header.append('Content-Type', 'application/json');
                        body = {
                            username: username,
                            password: 'cGFzc3dvcmQ='
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.http.post("https://deliveryappserver.herokuapp.com/api/userAuth", JSON.stringify(body), { headers: header }).toPromise()];
                    case 2:
                        response = _a.sent();
                        return [2 /*return*/, response.json()];
                    case 3:
                        err_1 = _a.sent();
                        console.dir(err_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    LoginServiceService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], LoginServiceService);
    return LoginServiceService;
}());
exports.LoginServiceService = LoginServiceService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tc2VydmljZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4tc2VydmljZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBQzNDLHNDQUE4QztBQUM5Qyx3REFBMEQ7QUFDMUQsdUNBQXFDO0FBR3JDO0lBR0ksNkJBQW9CLElBQVU7UUFBVixTQUFJLEdBQUosSUFBSSxDQUFNO1FBRjlCLFFBQUcsR0FBVyxFQUFFLENBQUM7UUFHYixJQUFJLENBQUMsR0FBRyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVLLHNDQUFRLEdBQWQsVUFBZSxRQUFnQixFQUFFLFFBQWdCOzs7Ozs7d0JBQzdDLElBQUksQ0FBQyxHQUFHLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDdkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQ2xCLE1BQU0sR0FBRyxJQUFJLGNBQU8sRUFBRSxDQUFDO3dCQUMzQixNQUFNLENBQUMsTUFBTSxDQUFDLGNBQWMsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO3dCQUU1QyxJQUFJLEdBQUc7NEJBQ1QsUUFBUSxFQUFFLFFBQVE7NEJBQ2xCLFFBQVEsRUFBRSxjQUFjO3lCQUMzQixDQUFDOzs7O3dCQUdpQixxQkFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxzREFBc0QsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsT0FBTyxFQUFFLE1BQU0sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFLEVBQUE7O3dCQUE1SSxRQUFRLEdBQUcsU0FBaUk7d0JBQ2hKLHNCQUFPLFFBQVEsQ0FBQyxJQUFJLEVBQUUsRUFBQzs7O3dCQUd2QixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUcsQ0FBQyxDQUFDOzs7Ozs7S0FFeEI7SUF6QlEsbUJBQW1CO1FBRC9CLGlCQUFVLEVBQUU7eUNBSWlCLFdBQUk7T0FIckIsbUJBQW1CLENBMkIvQjtJQUFELDBCQUFDO0NBQUEsQUEzQkQsSUEyQkM7QUEzQlksa0RBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cCwgSGVhZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xuaW1wb3J0ICogYXMgbG9jYWxTdG9yYWdlIGZyb20gJ25hdGl2ZXNjcmlwdC1sb2NhbHN0b3JhZ2UnO1xuaW1wb3J0ICdyeGpzL2FkZC9vcGVyYXRvci90b1Byb21pc2UnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgTG9naW5TZXJ2aWNlU2VydmljZSB7XG4gICAgdXJsOiBzdHJpbmcgPSAnJztcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cCkge1xuICAgICAgICB0aGlzLnVybCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1cmwnKTtcbiAgICB9XG5cbiAgICBhc3luYyB1c2VyQXV0aCh1c2VybmFtZTogc3RyaW5nLCBwYXNzd29yZDogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMudXJsID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VybCcpO1xuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnVybCk7XG4gICAgICAgIGxldCBoZWFkZXIgPSBuZXcgSGVhZGVycygpO1xuICAgICAgICBoZWFkZXIuYXBwZW5kKCdDb250ZW50LVR5cGUnLCAnYXBwbGljYXRpb24vanNvbicpO1xuXG4gICAgICAgIGNvbnN0IGJvZHkgPSB7XG4gICAgICAgICAgICB1c2VybmFtZTogdXNlcm5hbWUsXG4gICAgICAgICAgICBwYXNzd29yZDogJ2NHRnpjM2R2Y21RPSdcbiAgICAgICAgfTtcblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgbGV0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5odHRwLnBvc3QoYGh0dHBzOi8vZGVsaXZlcnlhcHBzZXJ2ZXIuaGVyb2t1YXBwLmNvbS9hcGkvdXNlckF1dGhgLCBKU09OLnN0cmluZ2lmeShib2R5KSwge2hlYWRlcnM6IGhlYWRlcn0pLnRvUHJvbWlzZSgpO1xuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmpzb24oKTtcbiAgICAgICAgfVxuICAgICAgICBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICBjb25zb2xlLmRpcihlcnIpO1xuICAgICAgICB9XG4gICAgfVxuXG59Il19