import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import * as localStorage from 'nativescript-localstorage';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class OrderService {

    url: string = '';

    constructor(private http: Http) {
        this.url = localStorage.getItem('url');
    }

    async userAuth(body, methodName) {
        this.url = localStorage.getItem('url');
        console.log(this.url);
        if (!this.url) {
            alert('API end point is empty. Go to settings to set it');
        }
        let header = new Headers();
        header.append('Content-Type', 'application/json');

        try {
            let response = await this.http.post(`${this.url}/${methodName}`, JSON.stringify(body), { headers: header }).toPromise();
            return response.json();
        }
        catch (err) {
            console.dir(err);
        }
    }

}