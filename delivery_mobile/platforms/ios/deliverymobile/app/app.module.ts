import { registerElement } from 'nativescript-angular/element-registry';
registerElement('DrawingPad', () => require('nativescript-drawingpad').DrawingPad);

import { NgModule, NgModuleFactoryLoader, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NSModuleFactoryLoader } from "nativescript-angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from 'nativescript-angular/forms';

// import { AppRoutingModule } from "./app-routing.module";
import { routes, navcomponents } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { LoginServiceService } from './services/login-service.service';
import { OrderService } from './services/order.service';

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forRoot(routes),
        NativeScriptHttpModule, 
        NativeScriptFormsModule
        // AppRoutingModule
    ],
    declarations: [
        AppComponent,
        ...navcomponents
    ],
    providers: [
        { provide: NgModuleFactoryLoader, useClass: NSModuleFactoryLoader },
        LoginServiceService,
        OrderService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
