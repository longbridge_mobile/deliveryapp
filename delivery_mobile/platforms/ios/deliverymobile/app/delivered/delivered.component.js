"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var order_service_1 = require("../services/order.service");
var router_1 = require("nativescript-angular/router");
var DeliveredComponent = /** @class */ (function () {
    function DeliveredComponent(orderService, routerExt) {
        this.orderService = orderService;
        this.routerExt = routerExt;
        this.message = '';
        this.source = [];
        this.isBusy = false;
    }
    DeliveredComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getAgentOrders()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DeliveredComponent.prototype.onItemTap = function (data) {
        console.log(data.index);
        console.dir(this.source[data.index]);
        var navigationExtras = {
            queryParams: {
                "data": JSON.stringify(this.source[data.index]),
                "status": "D",
                "statusText": "Delivered"
            }
        };
        this.routerExt.navigate(['/order-details'], navigationExtras);
    };
    DeliveredComponent.prototype.getAgentOrders = function () {
        return __awaiter(this, void 0, void 0, function () {
            var body, response, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.busyToggle();
                        this.message = '';
                        body = {
                            agentId: 1,
                            orderStatus: 'D'
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.orderService.userAuth(body, 'agentOrders')];
                    case 2:
                        response = _a.sent();
                        if (response.status === 200) {
                            this.source = response.data;
                        }
                        else {
                            this.message = 'You have no pending orders';
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        err_1 = _a.sent();
                        console.log(err_1);
                        return [3 /*break*/, 4];
                    case 4:
                        this.busyToggle();
                        return [2 /*return*/];
                }
            });
        });
    };
    DeliveredComponent.prototype.busyToggle = function () {
        this.isBusy = !this.isBusy;
    };
    DeliveredComponent = __decorate([
        core_1.Component({
            selector: 'delivered',
            moduleId: module.id,
            templateUrl: './delivered.component.html',
            styleUrls: ['./delivered.common.css']
        }),
        __metadata("design:paramtypes", [order_service_1.OrderService,
            router_1.RouterExtensions])
    ], DeliveredComponent);
    return DeliveredComponent;
}());
exports.DeliveredComponent = DeliveredComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmVkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRlbGl2ZXJlZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFDbEQsMkRBQXlEO0FBQ3pELHNEQUErRDtBQVUvRDtJQUtDLDRCQUNTLFlBQTBCLEVBQzFCLFNBQTJCO1FBRDNCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzFCLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBTnBDLFlBQU8sR0FBVyxFQUFFLENBQUM7UUFDckIsV0FBTSxHQUFlLEVBQUUsQ0FBQztRQUN4QixXQUFNLEdBQVksS0FBSyxDQUFDO0lBS3BCLENBQUM7SUFFQyxxQ0FBUSxHQUFkOzs7OzRCQUNDLHFCQUFNLElBQUksQ0FBQyxjQUFjLEVBQUUsRUFBQTs7d0JBQTNCLFNBQTJCLENBQUM7Ozs7O0tBQzVCO0lBRUQsc0NBQVMsR0FBVCxVQUFVLElBQUk7UUFDYixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN4QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDckMsSUFBSSxnQkFBZ0IsR0FBcUI7WUFDeEMsV0FBVyxFQUFFO2dCQUNaLE1BQU0sRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUMvQyxRQUFRLEVBQUUsR0FBRztnQkFDYixZQUFZLEVBQUUsV0FBVzthQUN6QjtTQUNELENBQUM7UUFFRixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBRUssMkNBQWMsR0FBcEI7Ozs7Ozt3QkFDQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7d0JBQ2xCLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO3dCQUNkLElBQUksR0FBRzs0QkFDVixPQUFPLEVBQUUsQ0FBQzs0QkFDVixXQUFXLEVBQUUsR0FBRzt5QkFDaEIsQ0FBQzs7Ozt3QkFHYyxxQkFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDLEVBQUE7O3dCQUFoRSxRQUFRLEdBQUcsU0FBcUQ7d0JBQ3BFLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQzs0QkFDN0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFBO3dCQUM1QixDQUFDO3dCQUFDLElBQUksQ0FBQyxDQUFDOzRCQUNQLElBQUksQ0FBQyxPQUFPLEdBQUcsNEJBQTRCLENBQUM7d0JBQzdDLENBQUM7Ozs7d0JBR0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFHLENBQUMsQ0FBQzs7O3dCQUVsQixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7Ozs7O0tBQ2xCO0lBRUQsdUNBQVUsR0FBVjtRQUNDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQzVCLENBQUM7SUFwRFcsa0JBQWtCO1FBUDlCLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsV0FBVztZQUNyQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLDRCQUE0QjtZQUN6QyxTQUFTLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQztTQUNyQyxDQUFDO3lDQVFzQiw0QkFBWTtZQUNmLHlCQUFnQjtPQVB4QixrQkFBa0IsQ0FxRDlCO0lBQUQseUJBQUM7Q0FBQSxBQXJERCxJQXFEQztBQXJEWSxnREFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgT3JkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvb3JkZXIuc2VydmljZSc7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IE5hdmlnYXRpb25FeHRyYXMgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuXG5AQ29tcG9uZW50KHtcblx0c2VsZWN0b3I6ICdkZWxpdmVyZWQnLFxuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxuXHR0ZW1wbGF0ZVVybDogJy4vZGVsaXZlcmVkLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vZGVsaXZlcmVkLmNvbW1vbi5jc3MnXVxufSlcblxuZXhwb3J0IGNsYXNzIERlbGl2ZXJlZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cdG1lc3NhZ2U6IHN0cmluZyA9ICcnO1xuXHRzb3VyY2U6IEFycmF5PGFueT4gPSBbXTtcblx0aXNCdXN5OiBib29sZWFuID0gZmFsc2U7XG5cblx0Y29uc3RydWN0b3IoXG5cdFx0cHJpdmF0ZSBvcmRlclNlcnZpY2U6IE9yZGVyU2VydmljZSxcblx0XHRwcml2YXRlIHJvdXRlckV4dDogUm91dGVyRXh0ZW5zaW9uc1xuXHQpIHsgfVxuXG5cdGFzeW5jIG5nT25Jbml0KCkge1xuXHRcdGF3YWl0IHRoaXMuZ2V0QWdlbnRPcmRlcnMoKTtcblx0fVxuXG5cdG9uSXRlbVRhcChkYXRhKSB7XG5cdFx0Y29uc29sZS5sb2coZGF0YS5pbmRleCk7XG5cdFx0Y29uc29sZS5kaXIodGhpcy5zb3VyY2VbZGF0YS5pbmRleF0pO1xuXHRcdGxldCBuYXZpZ2F0aW9uRXh0cmFzOiBOYXZpZ2F0aW9uRXh0cmFzID0ge1xuXHRcdFx0cXVlcnlQYXJhbXM6IHtcblx0XHRcdFx0XCJkYXRhXCI6IEpTT04uc3RyaW5naWZ5KHRoaXMuc291cmNlW2RhdGEuaW5kZXhdKSxcblx0XHRcdFx0XCJzdGF0dXNcIjogXCJEXCIsXG5cdFx0XHRcdFwic3RhdHVzVGV4dFwiOiBcIkRlbGl2ZXJlZFwiXG5cdFx0XHR9XG5cdFx0fTtcblxuXHRcdHRoaXMucm91dGVyRXh0Lm5hdmlnYXRlKFsnL29yZGVyLWRldGFpbHMnXSwgbmF2aWdhdGlvbkV4dHJhcyk7XG5cdH1cblxuXHRhc3luYyBnZXRBZ2VudE9yZGVycygpIHtcblx0XHR0aGlzLmJ1c3lUb2dnbGUoKTtcblx0XHR0aGlzLm1lc3NhZ2UgPSAnJztcblx0XHRsZXQgYm9keSA9IHtcblx0XHRcdGFnZW50SWQ6IDEsXG5cdFx0XHRvcmRlclN0YXR1czogJ0QnXG5cdFx0fTtcblxuXHRcdHRyeSB7XG5cdFx0XHRsZXQgcmVzcG9uc2UgPSBhd2FpdCB0aGlzLm9yZGVyU2VydmljZS51c2VyQXV0aChib2R5LCAnYWdlbnRPcmRlcnMnKTtcblx0XHRcdGlmIChyZXNwb25zZS5zdGF0dXMgPT09IDIwMCkge1xuXHRcdFx0XHR0aGlzLnNvdXJjZSA9IHJlc3BvbnNlLmRhdGFcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHRoaXMubWVzc2FnZSA9ICdZb3UgaGF2ZSBubyBwZW5kaW5nIG9yZGVycyc7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdGNhdGNoIChlcnIpIHtcblx0XHRcdGNvbnNvbGUubG9nKGVycik7XG5cdFx0fVxuXHRcdHRoaXMuYnVzeVRvZ2dsZSgpO1xuXHR9XG5cblx0YnVzeVRvZ2dsZSgpIHtcblx0XHR0aGlzLmlzQnVzeSA9ICF0aGlzLmlzQnVzeTtcblx0fVxufSJdfQ==