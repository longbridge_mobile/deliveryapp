import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class OrderService {

    url: string = 'http://192.168.1.3:3000/api';

    constructor(private http: Http) { }

    async userAuth(body, methodName) {
        let header = new Headers();
        header.append('Content-Type', 'application/json');

        try {
            let response = await this.http.post(`${this.url}/${methodName}`, JSON.stringify(body), { headers: header }).toPromise();
            return response.json();
        }
        catch (err) {
            console.dir(err);
        }
    }

}