import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class LoginServiceService {
    url: string = 'http://192.168.1.3:3000/api/userAuth';

    constructor(private http: Http) { }

    async userAuth(username: string, password: string) {
        let header = new Headers();
        header.append('Content-Type', 'application/json');

        const body = {
            username: username,
            password: password
        };

        try {
            let response = await this.http.post(this.url, JSON.stringify(body), {headers: header}).toPromise();
            return response.json();
        }
        catch (err) {
            console.dir(err);
        }
    }

}