"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var OrderService = /** @class */ (function () {
    function OrderService(http) {
        this.http = http;
        this.url = 'http://192.168.1.3:3000/api';
    }
    OrderService.prototype.userAuth = function (body, methodName) {
        return __awaiter(this, void 0, void 0, function () {
            var header, response, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        header = new http_1.Headers();
                        header.append('Content-Type', 'application/json');
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.http.post(this.url + "/" + methodName, JSON.stringify(body), { headers: header }).toPromise()];
                    case 2:
                        response = _a.sent();
                        return [2 /*return*/, response.json()];
                    case 3:
                        err_1 = _a.sent();
                        console.dir(err_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    OrderService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], OrderService);
    return OrderService;
}());
exports.OrderService = OrderService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3JkZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm9yZGVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0Msc0NBQThDO0FBQzlDLHVDQUFxQztBQUdyQztJQUlJLHNCQUFvQixJQUFVO1FBQVYsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUY5QixRQUFHLEdBQVcsNkJBQTZCLENBQUM7SUFFVixDQUFDO0lBRTdCLCtCQUFRLEdBQWQsVUFBZSxJQUFJLEVBQUUsVUFBVTs7Ozs7O3dCQUN2QixNQUFNLEdBQUcsSUFBSSxjQUFPLEVBQUUsQ0FBQzt3QkFDM0IsTUFBTSxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsa0JBQWtCLENBQUMsQ0FBQzs7Ozt3QkFHL0IscUJBQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUksSUFBSSxDQUFDLEdBQUcsU0FBSSxVQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxFQUFBOzt3QkFBbkgsUUFBUSxHQUFHLFNBQXdHO3dCQUN2SCxzQkFBTyxRQUFRLENBQUMsSUFBSSxFQUFFLEVBQUM7Ozt3QkFHdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFHLENBQUMsQ0FBQzs7Ozs7O0tBRXhCO0lBakJRLFlBQVk7UUFEeEIsaUJBQVUsRUFBRTt5Q0FLaUIsV0FBSTtPQUpyQixZQUFZLENBbUJ4QjtJQUFELG1CQUFDO0NBQUEsQUFuQkQsSUFtQkM7QUFuQlksb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwLCBIZWFkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XG5pbXBvcnQgJ3J4anMvYWRkL29wZXJhdG9yL3RvUHJvbWlzZSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBPcmRlclNlcnZpY2Uge1xuXG4gICAgdXJsOiBzdHJpbmcgPSAnaHR0cDovLzE5Mi4xNjguMS4zOjMwMDAvYXBpJztcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cCkgeyB9XG5cbiAgICBhc3luYyB1c2VyQXV0aChib2R5LCBtZXRob2ROYW1lKSB7XG4gICAgICAgIGxldCBoZWFkZXIgPSBuZXcgSGVhZGVycygpO1xuICAgICAgICBoZWFkZXIuYXBwZW5kKCdDb250ZW50LVR5cGUnLCAnYXBwbGljYXRpb24vanNvbicpO1xuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBsZXQgcmVzcG9uc2UgPSBhd2FpdCB0aGlzLmh0dHAucG9zdChgJHt0aGlzLnVybH0vJHttZXRob2ROYW1lfWAsIEpTT04uc3RyaW5naWZ5KGJvZHkpLCB7IGhlYWRlcnM6IGhlYWRlciB9KS50b1Byb21pc2UoKTtcbiAgICAgICAgICAgIHJldHVybiByZXNwb25zZS5qc29uKCk7XG4gICAgICAgIH1cbiAgICAgICAgY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgY29uc29sZS5kaXIoZXJyKTtcbiAgICAgICAgfVxuICAgIH1cblxufSJdfQ==