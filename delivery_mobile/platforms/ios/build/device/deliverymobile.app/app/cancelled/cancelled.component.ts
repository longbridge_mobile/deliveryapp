import { Component, OnInit } from '@angular/core';
import { OrderService } from '../services/order.service';
import { RouterExtensions } from 'nativescript-angular/router';
import { NavigationExtras } from '@angular/router';

@Component({
	selector: 'cancelled',
	moduleId: module.id,
	templateUrl: './cancelled.component.html',
	styleUrls: ['./cancelled.common.css']
})

export class CancelledComponent implements OnInit {
	message: string = '';
	source: Array<any> = [];
	isBusy: boolean = false;

	constructor(
		private orderService: OrderService,
		private routerExt: RouterExtensions
	) { }

	async ngOnInit() {
		await this.getAgentOrders();
	}

	onItemTap(data) {
		console.log(data.index);
		console.dir(this.source[data.index]);
		let navigationExtras: NavigationExtras = {
			queryParams: {
				"data": JSON.stringify(this.source[data.index]),
				"status": "C",
				"statusText": "Cancelled"
			}
		};

		this.routerExt.navigate(['/order-details'], navigationExtras);
	}

	async getAgentOrders() {
		this.busyToggle();
		this.message = '';
		let body = {
			agentId: 1,
			orderStatus: 'C'
		};

		try {
			let response = await this.orderService.userAuth(body, 'agentOrders');
			if (response.status === 200) {
				this.source = response.data
			} else {
				this.message = 'You have no pending orders';
			}
		}
		catch (err) {
			console.log(err);
		}
		this.busyToggle();
	}

	busyToggle() {
		this.isBusy = !this.isBusy;
	}
}