"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dialogModule = require("ui/dialogs");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var signature_pad_component_1 = require("../signature-pad/signature-pad.component");
var imageSource = require("image-source");
var router_1 = require("@angular/router");
var router_2 = require("nativescript-angular/router");
var order_service_1 = require("../services/order.service");
var OrderDetailsComponent = /** @class */ (function () {
    function OrderDetailsComponent(modalService, vcRef, router, orderService, routerExt) {
        this.modalService = modalService;
        this.vcRef = vcRef;
        this.router = router;
        this.orderService = orderService;
        this.routerExt = routerExt;
        this.isLoading = false;
        this.statusText = '';
        this.status = '';
    }
    OrderDetailsComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.router.queryParams.subscribe(function (params) {
                    _this.data = JSON.parse(params['data']);
                    _this.statusText = params['statusText'];
                    _this.status = params['status'];
                });
                return [2 /*return*/];
            });
        });
    };
    OrderDetailsComponent.prototype.cancelOrder = function () {
        var _this = this;
        dialogModule.confirm('Are you sure you want to cancel?').then(function (val) {
            console.log(val);
            if (val) {
                _this.updateOrder('C', _this.data.id, 'ORDER CANCELLED!');
            }
        });
    };
    OrderDetailsComponent.prototype.deliverOrder = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var options;
            return __generator(this, function (_a) {
                options = {
                    viewContainerRef: this.vcRef,
                    fullscreen: false
                };
                this.modalService.showModal(signature_pad_component_1.SignaturePadComponent, options)
                    .then(function (data) {
                    if (data) {
                        var img = imageSource.fromNativeSource(data);
                        console.log(img.toBase64String('png'));
                        _this.updateOrder('D', _this.data.id, 'SUCCESS!');
                    }
                }).catch(function (err) { return console.log(err); });
                return [2 /*return*/];
            });
        });
    };
    OrderDetailsComponent.prototype.updateOrder = function (status, id, message) {
        return __awaiter(this, void 0, void 0, function () {
            var body, response, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        body = {
                            status: status,
                            id: id
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.orderService.userAuth(body, 'updateOrder')];
                    case 2:
                        response = _a.sent();
                        if (response.status === 200) {
                            alert(message);
                            this.routerExt.navigate(['/dashboard'], { clearHistory: true });
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        err_1 = _a.sent();
                        console.log(err_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    OrderDetailsComponent = __decorate([
        core_1.Component({
            selector: 'order-details',
            moduleId: module.id,
            templateUrl: './order-details.component.html',
            styleUrls: ['./order-details.common.css']
        }),
        __metadata("design:paramtypes", [modal_dialog_1.ModalDialogService,
            core_1.ViewContainerRef,
            router_1.ActivatedRoute,
            order_service_1.OrderService,
            router_2.RouterExtensions])
    ], OrderDetailsComponent);
    return OrderDetailsComponent;
}());
exports.OrderDetailsComponent = OrderDetailsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3JkZXItZGV0YWlscy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJvcmRlci1kZXRhaWxzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFvRTtBQUNwRSx5Q0FBMkM7QUFDM0Msa0VBQTJGO0FBQzNGLG9GQUFpRjtBQUNqRiwwQ0FBNEM7QUFDNUMsMENBQWlEO0FBQ2pELHNEQUErRDtBQUMvRCwyREFBeUQ7QUFTekQ7SUFNQywrQkFDUyxZQUFnQyxFQUNoQyxLQUF1QixFQUN2QixNQUFzQixFQUN0QixZQUEwQixFQUMxQixTQUEyQjtRQUozQixpQkFBWSxHQUFaLFlBQVksQ0FBb0I7UUFDaEMsVUFBSyxHQUFMLEtBQUssQ0FBa0I7UUFDdkIsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7UUFDdEIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFWcEMsY0FBUyxHQUFZLEtBQUssQ0FBQztRQUMzQixlQUFVLEdBQVcsRUFBRSxDQUFDO1FBQ3hCLFdBQU0sR0FBVyxFQUFFLENBQUM7SUFTaEIsQ0FBQztJQUVDLHdDQUFRLEdBQWQ7Ozs7Z0JBQ0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFVBQUEsTUFBTTtvQkFDdkMsS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO29CQUN2QyxLQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztvQkFDdkMsS0FBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ2hDLENBQUMsQ0FBQyxDQUFDOzs7O0tBQ0g7SUFFRCwyQ0FBVyxHQUFYO1FBQUEsaUJBUUM7UUFQQSxZQUFZLENBQUMsT0FBTyxDQUFDLGtDQUFrQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsR0FBWTtZQUMxRSxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRWpCLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ1QsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLEVBQUUsS0FBSSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztZQUN6RCxDQUFDO1FBQ0YsQ0FBQyxDQUFDLENBQUM7SUFDSixDQUFDO0lBRUssNENBQVksR0FBbEI7Ozs7O2dCQUNLLE9BQU8sR0FBdUI7b0JBQ2pDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxLQUFLO29CQUM1QixVQUFVLEVBQUUsS0FBSztpQkFDakIsQ0FBQztnQkFFRixJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQywrQ0FBcUIsRUFBRSxPQUFPLENBQUM7cUJBQzFELElBQUksQ0FBQyxVQUFDLElBQUk7b0JBQ1YsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDVixJQUFJLEdBQUcsR0FBRyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQzdDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO3dCQUN2QyxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsQ0FBQztvQkFDakQsQ0FBQztnQkFDRixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFoQixDQUFnQixDQUFDLENBQUM7Ozs7S0FDbEM7SUFFSywyQ0FBVyxHQUFqQixVQUFrQixNQUFNLEVBQUUsRUFBRSxFQUFFLE9BQWU7Ozs7Ozt3QkFDeEMsSUFBSSxHQUFHOzRCQUNWLE1BQU0sRUFBRSxNQUFNOzRCQUNkLEVBQUUsRUFBRSxFQUFFO3lCQUNOLENBQUM7Ozs7d0JBR2MscUJBQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxFQUFBOzt3QkFBaEUsUUFBUSxHQUFHLFNBQXFEO3dCQUNwRSxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUM7NEJBQzdCLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQzs0QkFDZixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxFQUFFLEVBQUMsWUFBWSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUM7d0JBQy9ELENBQUM7Ozs7d0JBRUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFHLENBQUMsQ0FBQzs7Ozs7O0tBRWxCO0lBL0RXLHFCQUFxQjtRQVBqQyxnQkFBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLGVBQWU7WUFDekIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSxnQ0FBZ0M7WUFDN0MsU0FBUyxFQUFFLENBQUMsNEJBQTRCLENBQUM7U0FDekMsQ0FBQzt5Q0FTc0IsaUNBQWtCO1lBQ3pCLHVCQUFnQjtZQUNmLHVCQUFjO1lBQ1IsNEJBQVk7WUFDZix5QkFBZ0I7T0FYeEIscUJBQXFCLENBZ0VqQztJQUFELDRCQUFDO0NBQUEsQUFoRUQsSUFnRUM7QUFoRVksc0RBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIFZpZXdDb250YWluZXJSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCAqIGFzIGRpYWxvZ01vZHVsZSBmcm9tICd1aS9kaWFsb2dzJztcbmltcG9ydCB7IE1vZGFsRGlhbG9nU2VydmljZSwgTW9kYWxEaWFsb2dPcHRpb25zIH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXIvbW9kYWwtZGlhbG9nJztcbmltcG9ydCB7IFNpZ25hdHVyZVBhZENvbXBvbmVudCB9IGZyb20gJy4uL3NpZ25hdHVyZS1wYWQvc2lnbmF0dXJlLXBhZC5jb21wb25lbnQnO1xuaW1wb3J0ICogYXMgaW1hZ2VTb3VyY2UgZnJvbSAnaW1hZ2Utc291cmNlJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tICduYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgT3JkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvb3JkZXIuc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuXHRzZWxlY3RvcjogJ29yZGVyLWRldGFpbHMnLFxuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxuXHR0ZW1wbGF0ZVVybDogJy4vb3JkZXItZGV0YWlscy5jb21wb25lbnQuaHRtbCcsXG5cdHN0eWxlVXJsczogWycuL29yZGVyLWRldGFpbHMuY29tbW9uLmNzcyddXG59KVxuXG5leHBvcnQgY2xhc3MgT3JkZXJEZXRhaWxzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblx0aXNMb2FkaW5nOiBib29sZWFuID0gZmFsc2U7XG5cdHN0YXR1c1RleHQ6IHN0cmluZyA9ICcnO1xuXHRzdGF0dXM6IHN0cmluZyA9ICcnO1xuXHRkYXRhOiBhbnk7XG5cblx0Y29uc3RydWN0b3IoXG5cdFx0cHJpdmF0ZSBtb2RhbFNlcnZpY2U6IE1vZGFsRGlhbG9nU2VydmljZSxcblx0XHRwcml2YXRlIHZjUmVmOiBWaWV3Q29udGFpbmVyUmVmLFxuXHRcdHByaXZhdGUgcm91dGVyOiBBY3RpdmF0ZWRSb3V0ZSxcblx0XHRwcml2YXRlIG9yZGVyU2VydmljZTogT3JkZXJTZXJ2aWNlLFxuXHRcdHByaXZhdGUgcm91dGVyRXh0OiBSb3V0ZXJFeHRlbnNpb25zXG5cdCkgeyB9XG5cblx0YXN5bmMgbmdPbkluaXQoKSB7XG5cdFx0dGhpcy5yb3V0ZXIucXVlcnlQYXJhbXMuc3Vic2NyaWJlKHBhcmFtcyA9PiB7XG5cdFx0XHR0aGlzLmRhdGEgPSBKU09OLnBhcnNlKHBhcmFtc1snZGF0YSddKTtcblx0XHRcdHRoaXMuc3RhdHVzVGV4dCA9IHBhcmFtc1snc3RhdHVzVGV4dCddO1xuXHRcdFx0dGhpcy5zdGF0dXMgPSBwYXJhbXNbJ3N0YXR1cyddO1xuXHRcdH0pO1xuXHR9XG5cblx0Y2FuY2VsT3JkZXIoKTogdm9pZCB7XG5cdFx0ZGlhbG9nTW9kdWxlLmNvbmZpcm0oJ0FyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBjYW5jZWw/JykudGhlbigodmFsOiBib29sZWFuKT0+e1xuXHRcdFx0Y29uc29sZS5sb2codmFsKTtcblxuXHRcdFx0aWYgKHZhbCkge1xuXHRcdFx0XHR0aGlzLnVwZGF0ZU9yZGVyKCdDJywgdGhpcy5kYXRhLmlkLCAnT1JERVIgQ0FOQ0VMTEVEIScpO1xuXHRcdFx0fVxuXHRcdH0pO1xuXHR9XG5cblx0YXN5bmMgZGVsaXZlck9yZGVyKCkge1xuXHRcdGxldCBvcHRpb25zOiBNb2RhbERpYWxvZ09wdGlvbnMgPSB7XG5cdFx0XHR2aWV3Q29udGFpbmVyUmVmOiB0aGlzLnZjUmVmLFxuXHRcdFx0ZnVsbHNjcmVlbjogZmFsc2Vcblx0XHR9O1xuXHRcdFxuXHRcdHRoaXMubW9kYWxTZXJ2aWNlLnNob3dNb2RhbChTaWduYXR1cmVQYWRDb21wb25lbnQsIG9wdGlvbnMpXG5cdFx0LnRoZW4oKGRhdGEpID0+IHtcblx0XHRcdGlmIChkYXRhKSB7XG5cdFx0XHRcdGxldCBpbWcgPSBpbWFnZVNvdXJjZS5mcm9tTmF0aXZlU291cmNlKGRhdGEpO1xuXHRcdFx0XHRjb25zb2xlLmxvZyhpbWcudG9CYXNlNjRTdHJpbmcoJ3BuZycpKTtcblx0XHRcdFx0dGhpcy51cGRhdGVPcmRlcignRCcsIHRoaXMuZGF0YS5pZCwgJ1NVQ0NFU1MhJyk7XG5cdFx0XHR9XG5cdFx0fSkuY2F0Y2goZXJyID0+IGNvbnNvbGUubG9nKGVycikpO1xuXHR9XG5cblx0YXN5bmMgdXBkYXRlT3JkZXIoc3RhdHVzLCBpZCwgbWVzc2FnZTogc3RyaW5nKSB7XG5cdFx0bGV0IGJvZHkgPSB7XG5cdFx0XHRzdGF0dXM6IHN0YXR1cyxcblx0XHRcdGlkOiBpZFxuXHRcdH07XG5cblx0XHR0cnkge1xuXHRcdFx0bGV0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5vcmRlclNlcnZpY2UudXNlckF1dGgoYm9keSwgJ3VwZGF0ZU9yZGVyJyk7XG5cdFx0XHRpZiAocmVzcG9uc2Uuc3RhdHVzID09PSAyMDApIHtcblx0XHRcdFx0YWxlcnQobWVzc2FnZSk7XG5cdFx0XHRcdHRoaXMucm91dGVyRXh0Lm5hdmlnYXRlKFsnL2Rhc2hib2FyZCddLCB7Y2xlYXJIaXN0b3J5OiB0cnVlfSk7XG5cdFx0XHR9XG5cdFx0fSBjYXRjaCAoZXJyKSB7XG5cdFx0XHRjb25zb2xlLmxvZyhlcnIpO1xuXHRcdH1cblx0fVxufSAiXX0=