import { Component, OnInit, ViewContainerRef } from '@angular/core';
import * as dialogModule from 'ui/dialogs';
import { ModalDialogService, ModalDialogOptions } from 'nativescript-angular/modal-dialog';
import { SignaturePadComponent } from '../signature-pad/signature-pad.component';
import * as imageSource from 'image-source';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { OrderService } from '../services/order.service';

@Component({
	selector: 'order-details',
	moduleId: module.id,
	templateUrl: './order-details.component.html',
	styleUrls: ['./order-details.common.css']
})

export class OrderDetailsComponent implements OnInit {
	isLoading: boolean = false;
	statusText: string = '';
	status: string = '';
	data: any;

	constructor(
		private modalService: ModalDialogService,
		private vcRef: ViewContainerRef,
		private router: ActivatedRoute,
		private orderService: OrderService,
		private routerExt: RouterExtensions
	) { }

	async ngOnInit() {
		this.router.queryParams.subscribe(params => {
			this.data = JSON.parse(params['data']);
			this.statusText = params['statusText'];
			this.status = params['status'];
		});
	}

	cancelOrder(): void {
		dialogModule.confirm('Are you sure you want to cancel?').then((val: boolean)=>{
			console.log(val);

			if (val) {
				this.updateOrder('C', this.data.id, 'ORDER CANCELLED!');
			}
		});
	}

	async deliverOrder() {
		let options: ModalDialogOptions = {
			viewContainerRef: this.vcRef,
			fullscreen: false
		};
		
		this.modalService.showModal(SignaturePadComponent, options)
		.then((data) => {
			if (data) {
				let img = imageSource.fromNativeSource(data);
				console.log(img.toBase64String('png'));
				this.updateOrder('D', this.data.id, 'SUCCESS!');
			}
		}).catch(err => console.log(err));
	}

	async updateOrder(status, id, message: string) {
		let body = {
			status: status,
			id: id
		};

		try {
			let response = await this.orderService.userAuth(body, 'updateOrder');
			if (response.status === 200) {
				alert(message);
				this.routerExt.navigate(['/dashboard'], {clearHistory: true});
			}
		} catch (err) {
			console.log(err);
		}
	}
} 