import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PendingComponent } from './pending/pending.component';
import { DeliveredComponent } from './delivered/delivered.component';
import { CancelledComponent } from './cancelled/cancelled.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { SignaturePadComponent } from './signature-pad/signature-pad.component';

export const routes: any = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", component: HomeComponent },
    { path: "dashboard", component: DashboardComponent },
    { path: "pending", component: PendingComponent },
    { path: "delivered", component: DeliveredComponent },
    { path: "cancelled", component: CancelledComponent },
    { path: "order-details", component: OrderDetailsComponent },
    { path: "signature", component: SignaturePadComponent },
];

export const navcomponents: any = [
    HomeComponent,
    DashboardComponent,
    PendingComponent,
    DeliveredComponent,
    CancelledComponent,
    OrderDetailsComponent,
    SignaturePadComponent
];

