"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var dialogModule = require("ui/dialogs");
var router_1 = require("nativescript-angular/router");
var login_service_service_1 = require("../services/login-service.service");
var HomeComponent = /** @class */ (function () {
    function HomeComponent(page, loginService, routerExt) {
        this.page = page;
        this.loginService = loginService;
        this.routerExt = routerExt;
        this.isPassword = true;
        this.username = 'username';
        this.password = 'password';
        this.isBusy = false;
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
    };
    HomeComponent.prototype.signIn = function () {
        return __awaiter(this, void 0, void 0, function () {
            var resp, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.busyToggle();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 5, , 6]);
                        if (!(this.username !== '' && this.password !== '')) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.loginService.userAuth(this.username.toLowerCase(), this.password)];
                    case 2:
                        resp = _a.sent();
                        if (resp.status === 200) {
                            this.routerExt.navigate(['/dashboard'], { clearHistory: true });
                        }
                        else {
                            dialogModule.alert('Invalid username or password');
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        dialogModule.alert('Username and Password are required!');
                        _a.label = 4;
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        err_1 = _a.sent();
                        console.log(err_1);
                        return [3 /*break*/, 6];
                    case 6:
                        this.busyToggle();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeComponent.prototype.passToggle = function () {
        this.isPassword = !this.isPassword;
    };
    HomeComponent.prototype.busyToggle = function () {
        this.isBusy = !this.isBusy;
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: "Home",
            moduleId: module.id,
            templateUrl: "./home.component.html",
            styleUrls: ["./home-common.css"]
        }),
        __metadata("design:paramtypes", [page_1.Page,
            login_service_service_1.LoginServiceService,
            router_1.RouterExtensions])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCxnQ0FBK0I7QUFDL0IseUNBQTJDO0FBQzNDLHNEQUErRDtBQUUvRCwyRUFBd0U7QUFTeEU7SUFNSSx1QkFDWSxJQUFVLEVBQ1YsWUFBaUMsRUFDakMsU0FBMkI7UUFGM0IsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUNWLGlCQUFZLEdBQVosWUFBWSxDQUFxQjtRQUNqQyxjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQVJ2QyxlQUFVLEdBQVksSUFBSSxDQUFDO1FBQzNCLGFBQVEsR0FBVyxVQUFVLENBQUM7UUFDOUIsYUFBUSxHQUFXLFVBQVUsQ0FBQztRQUM5QixXQUFNLEdBQVksS0FBSyxDQUFDO0lBTXJCLENBQUM7SUFFSixnQ0FBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO0lBQ3JDLENBQUM7SUFFSyw4QkFBTSxHQUFaOzs7Ozs7d0JBQ0ksSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDOzs7OzZCQUVWLENBQUEsSUFBSSxDQUFDLFFBQVEsS0FBSyxFQUFFLElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxFQUFFLENBQUEsRUFBNUMsd0JBQTRDO3dCQUNqQyxxQkFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBQTs7d0JBQW5GLElBQUksR0FBRyxTQUE0RTt3QkFDdkYsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDOzRCQUN0QixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxFQUFFLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ3BFLENBQUM7d0JBQUMsSUFBSSxDQUFDLENBQUM7NEJBQ0osWUFBWSxDQUFDLEtBQUssQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO3dCQUN2RCxDQUFDOzs7d0JBRUQsWUFBWSxDQUFDLEtBQUssQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDOzs7Ozt3QkFJOUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFHLENBQUMsQ0FBQzs7O3dCQUVyQixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7Ozs7O0tBQ3JCO0lBRUQsa0NBQVUsR0FBVjtRQUNJLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQ3ZDLENBQUM7SUFFRCxrQ0FBVSxHQUFWO1FBQ0ksSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDL0IsQ0FBQztJQTFDUSxhQUFhO1FBUHpCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTTtZQUNoQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLHVCQUF1QjtZQUNwQyxTQUFTLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQztTQUNuQyxDQUFDO3lDQVNvQixXQUFJO1lBQ0ksMkNBQW1CO1lBQ3RCLHlCQUFnQjtPQVQ5QixhQUFhLENBMkN6QjtJQUFELG9CQUFDO0NBQUEsQUEzQ0QsSUEyQ0M7QUEzQ1ksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInVpL3BhZ2VcIjtcbmltcG9ydCAqIGFzIGRpYWxvZ01vZHVsZSBmcm9tICd1aS9kaWFsb2dzJztcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tICduYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXInO1xuXG5pbXBvcnQgeyBMb2dpblNlcnZpY2VTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvbG9naW4tc2VydmljZS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6IFwiSG9tZVwiLFxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG4gICAgdGVtcGxhdGVVcmw6IFwiLi9ob21lLmNvbXBvbmVudC5odG1sXCIsXG4gICAgc3R5bGVVcmxzOiBbXCIuL2hvbWUtY29tbW9uLmNzc1wiXVxufSlcblxuZXhwb3J0IGNsYXNzIEhvbWVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIGlzUGFzc3dvcmQ6IGJvb2xlYW4gPSB0cnVlO1xuICAgIHVzZXJuYW1lOiBzdHJpbmcgPSAndXNlcm5hbWUnO1xuICAgIHBhc3N3b3JkOiBzdHJpbmcgPSAncGFzc3dvcmQnO1xuICAgIGlzQnVzeTogYm9vbGVhbiA9IGZhbHNlO1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByaXZhdGUgcGFnZTogUGFnZSxcbiAgICAgICAgcHJpdmF0ZSBsb2dpblNlcnZpY2U6IExvZ2luU2VydmljZVNlcnZpY2UsXG4gICAgICAgIHByaXZhdGUgcm91dGVyRXh0OiBSb3V0ZXJFeHRlbnNpb25zXG4gICAgKSB7fVxuXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgICAgIHRoaXMucGFnZS5hY3Rpb25CYXJIaWRkZW4gPSB0cnVlO1xuICAgIH1cbiAgICBcbiAgICBhc3luYyBzaWduSW4oKSB7XG4gICAgICAgIHRoaXMuYnVzeVRvZ2dsZSgpO1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgaWYgKHRoaXMudXNlcm5hbWUgIT09ICcnICYmIHRoaXMucGFzc3dvcmQgIT09ICcnKSB7XG4gICAgICAgICAgICAgICAgbGV0IHJlc3AgPSBhd2FpdCB0aGlzLmxvZ2luU2VydmljZS51c2VyQXV0aCh0aGlzLnVzZXJuYW1lLnRvTG93ZXJDYXNlKCksIHRoaXMucGFzc3dvcmQpO1xuICAgICAgICAgICAgICAgIGlmIChyZXNwLnN0YXR1cyA9PT0gMjAwKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyRXh0Lm5hdmlnYXRlKFsnL2Rhc2hib2FyZCddLCB7IGNsZWFySGlzdG9yeTogdHJ1ZSB9KTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBkaWFsb2dNb2R1bGUuYWxlcnQoJ0ludmFsaWQgdXNlcm5hbWUgb3IgcGFzc3dvcmQnKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGRpYWxvZ01vZHVsZS5hbGVydCgnVXNlcm5hbWUgYW5kIFBhc3N3b3JkIGFyZSByZXF1aXJlZCEnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhlcnIpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuYnVzeVRvZ2dsZSgpO1xuICAgIH1cblxuICAgIHBhc3NUb2dnbGUoKSB7XG4gICAgICAgIHRoaXMuaXNQYXNzd29yZCA9ICF0aGlzLmlzUGFzc3dvcmQ7XG4gICAgfVxuXG4gICAgYnVzeVRvZ2dsZSgpIHtcbiAgICAgICAgdGhpcy5pc0J1c3kgPSAhdGhpcy5pc0J1c3k7XG4gICAgfVxufVxuIl19