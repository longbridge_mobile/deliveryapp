"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var home_component_1 = require("./home/home.component");
var dashboard_component_1 = require("./dashboard/dashboard.component");
var pending_component_1 = require("./pending/pending.component");
var delivered_component_1 = require("./delivered/delivered.component");
var cancelled_component_1 = require("./cancelled/cancelled.component");
var order_details_component_1 = require("./order-details/order-details.component");
var signature_pad_component_1 = require("./signature-pad/signature-pad.component");
exports.routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", component: home_component_1.HomeComponent },
    { path: "dashboard", component: dashboard_component_1.DashboardComponent },
    { path: "pending", component: pending_component_1.PendingComponent },
    { path: "delivered", component: delivered_component_1.DeliveredComponent },
    { path: "cancelled", component: cancelled_component_1.CancelledComponent },
    { path: "order-details", component: order_details_component_1.OrderDetailsComponent },
    { path: "signature", component: signature_pad_component_1.SignaturePadComponent },
];
exports.navcomponents = [
    home_component_1.HomeComponent,
    dashboard_component_1.DashboardComponent,
    pending_component_1.PendingComponent,
    delivered_component_1.DeliveredComponent,
    cancelled_component_1.CancelledComponent,
    order_details_component_1.OrderDetailsComponent,
    signature_pad_component_1.SignaturePadComponent
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLXJvdXRpbmcubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwLXJvdXRpbmcubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsd0RBQXNEO0FBQ3RELHVFQUFxRTtBQUNyRSxpRUFBK0Q7QUFDL0QsdUVBQXFFO0FBQ3JFLHVFQUFxRTtBQUNyRSxtRkFBZ0Y7QUFDaEYsbUZBQWdGO0FBRW5FLFFBQUEsTUFBTSxHQUFRO0lBQ3ZCLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUU7SUFDcEQsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSw4QkFBYSxFQUFFO0lBQzFDLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUUsd0NBQWtCLEVBQUU7SUFDcEQsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxvQ0FBZ0IsRUFBRTtJQUNoRCxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLHdDQUFrQixFQUFFO0lBQ3BELEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUUsd0NBQWtCLEVBQUU7SUFDcEQsRUFBRSxJQUFJLEVBQUUsZUFBZSxFQUFFLFNBQVMsRUFBRSwrQ0FBcUIsRUFBRTtJQUMzRCxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLCtDQUFxQixFQUFFO0NBQzFELENBQUM7QUFFVyxRQUFBLGFBQWEsR0FBUTtJQUM5Qiw4QkFBYTtJQUNiLHdDQUFrQjtJQUNsQixvQ0FBZ0I7SUFDaEIsd0NBQWtCO0lBQ2xCLHdDQUFrQjtJQUNsQiwrQ0FBcUI7SUFDckIsK0NBQXFCO0NBQ3hCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBIb21lQ29tcG9uZW50IH0gZnJvbSAnLi9ob21lL2hvbWUuY29tcG9uZW50JztcbmltcG9ydCB7IERhc2hib2FyZENvbXBvbmVudCB9IGZyb20gJy4vZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgUGVuZGluZ0NvbXBvbmVudCB9IGZyb20gJy4vcGVuZGluZy9wZW5kaW5nLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBEZWxpdmVyZWRDb21wb25lbnQgfSBmcm9tICcuL2RlbGl2ZXJlZC9kZWxpdmVyZWQuY29tcG9uZW50JztcbmltcG9ydCB7IENhbmNlbGxlZENvbXBvbmVudCB9IGZyb20gJy4vY2FuY2VsbGVkL2NhbmNlbGxlZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgT3JkZXJEZXRhaWxzQ29tcG9uZW50IH0gZnJvbSAnLi9vcmRlci1kZXRhaWxzL29yZGVyLWRldGFpbHMuY29tcG9uZW50JztcbmltcG9ydCB7IFNpZ25hdHVyZVBhZENvbXBvbmVudCB9IGZyb20gJy4vc2lnbmF0dXJlLXBhZC9zaWduYXR1cmUtcGFkLmNvbXBvbmVudCc7XG5cbmV4cG9ydCBjb25zdCByb3V0ZXM6IGFueSA9IFtcbiAgICB7IHBhdGg6IFwiXCIsIHJlZGlyZWN0VG86IFwiL2hvbWVcIiwgcGF0aE1hdGNoOiBcImZ1bGxcIiB9LFxuICAgIHsgcGF0aDogXCJob21lXCIsIGNvbXBvbmVudDogSG9tZUNvbXBvbmVudCB9LFxuICAgIHsgcGF0aDogXCJkYXNoYm9hcmRcIiwgY29tcG9uZW50OiBEYXNoYm9hcmRDb21wb25lbnQgfSxcbiAgICB7IHBhdGg6IFwicGVuZGluZ1wiLCBjb21wb25lbnQ6IFBlbmRpbmdDb21wb25lbnQgfSxcbiAgICB7IHBhdGg6IFwiZGVsaXZlcmVkXCIsIGNvbXBvbmVudDogRGVsaXZlcmVkQ29tcG9uZW50IH0sXG4gICAgeyBwYXRoOiBcImNhbmNlbGxlZFwiLCBjb21wb25lbnQ6IENhbmNlbGxlZENvbXBvbmVudCB9LFxuICAgIHsgcGF0aDogXCJvcmRlci1kZXRhaWxzXCIsIGNvbXBvbmVudDogT3JkZXJEZXRhaWxzQ29tcG9uZW50IH0sXG4gICAgeyBwYXRoOiBcInNpZ25hdHVyZVwiLCBjb21wb25lbnQ6IFNpZ25hdHVyZVBhZENvbXBvbmVudCB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IG5hdmNvbXBvbmVudHM6IGFueSA9IFtcbiAgICBIb21lQ29tcG9uZW50LFxuICAgIERhc2hib2FyZENvbXBvbmVudCxcbiAgICBQZW5kaW5nQ29tcG9uZW50LFxuICAgIERlbGl2ZXJlZENvbXBvbmVudCxcbiAgICBDYW5jZWxsZWRDb21wb25lbnQsXG4gICAgT3JkZXJEZXRhaWxzQ29tcG9uZW50LFxuICAgIFNpZ25hdHVyZVBhZENvbXBvbmVudFxuXTtcblxuIl19