import { Component, OnInit } from "@angular/core";
import { Page } from "ui/page";
import * as dialogModule from 'ui/dialogs';
import { RouterExtensions } from 'nativescript-angular/router';

import { LoginServiceService } from '../services/login-service.service';

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ["./home-common.css"]
})

export class HomeComponent implements OnInit {
    isPassword: boolean = true;
    username: string = 'username';
    password: string = 'password';
    isBusy: boolean = false;

    constructor(
        private page: Page,
        private loginService: LoginServiceService,
        private routerExt: RouterExtensions
    ) {}

    ngOnInit(): void {
        this.page.actionBarHidden = true;
    }
    
    async signIn() {
        this.busyToggle();
        try {
            if (this.username !== '' && this.password !== '') {
                let resp = await this.loginService.userAuth(this.username.toLowerCase(), this.password);
                if (resp.status === 200) {
                    this.routerExt.navigate(['/dashboard'], { clearHistory: true });
                } else {
                    dialogModule.alert('Invalid username or password');
                }
            } else {
                dialogModule.alert('Username and Password are required!');
            }
        }
        catch (err) {
            console.log(err);
        }
        this.busyToggle();
    }

    passToggle() {
        this.isPassword = !this.isPassword;
    }

    busyToggle() {
        this.isBusy = !this.isBusy;
    }
}
