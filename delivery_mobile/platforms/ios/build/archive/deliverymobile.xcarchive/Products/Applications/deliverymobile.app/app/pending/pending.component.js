"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var order_service_1 = require("../services/order.service");
var router_1 = require("nativescript-angular/router");
var PendingComponent = /** @class */ (function () {
    function PendingComponent(orderService, routerExt) {
        this.orderService = orderService;
        this.routerExt = routerExt;
        this.message = '';
        this.source = [];
        this.isBusy = false;
    }
    PendingComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getAgentOrders()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PendingComponent.prototype.onItemTap = function (data) {
        var navigationExtras = {
            queryParams: {
                "data": JSON.stringify(this.source[data.index]),
                "status": "P",
                "statusText": "Pending"
            }
        };
        this.routerExt.navigate(['/order-details'], navigationExtras);
    };
    PendingComponent.prototype.getAgentOrders = function () {
        return __awaiter(this, void 0, void 0, function () {
            var body, response, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.busyToggle();
                        this.message = '';
                        body = {
                            agentId: 1,
                            orderStatus: 'P'
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.orderService.userAuth(body, 'agentOrders')];
                    case 2:
                        response = _a.sent();
                        if (response.status === 200) {
                            this.source = response.data;
                        }
                        else {
                            this.message = 'You have no pending orders';
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        err_1 = _a.sent();
                        console.log(err_1);
                        return [3 /*break*/, 4];
                    case 4:
                        this.busyToggle();
                        return [2 /*return*/];
                }
            });
        });
    };
    PendingComponent.prototype.busyToggle = function () {
        this.isBusy = !this.isBusy;
    };
    PendingComponent = __decorate([
        core_1.Component({
            selector: 'pending',
            moduleId: module.id,
            templateUrl: './pending.component.html',
            styleUrls: ['./pending.common.css']
        }),
        __metadata("design:paramtypes", [order_service_1.OrderService,
            router_1.RouterExtensions])
    ], PendingComponent);
    return PendingComponent;
}());
exports.PendingComponent = PendingComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVuZGluZy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJwZW5kaW5nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUVsRCwyREFBeUQ7QUFDekQsc0RBQStEO0FBVS9EO0lBS0MsMEJBQ1MsWUFBMEIsRUFDMUIsU0FBMkI7UUFEM0IsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFOcEMsWUFBTyxHQUFXLEVBQUUsQ0FBQztRQUNyQixXQUFNLEdBQWUsRUFBRSxDQUFDO1FBQ3hCLFdBQU0sR0FBWSxLQUFLLENBQUM7SUFLcEIsQ0FBQztJQUVDLG1DQUFRLEdBQWQ7Ozs7NEJBQ0MscUJBQU0sSUFBSSxDQUFDLGNBQWMsRUFBRSxFQUFBOzt3QkFBM0IsU0FBMkIsQ0FBQzs7Ozs7S0FDNUI7SUFFRCxvQ0FBUyxHQUFULFVBQVUsSUFBSTtRQUNiLElBQUksZ0JBQWdCLEdBQXFCO1lBQ3hDLFdBQVcsRUFBRTtnQkFDWixNQUFNLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDL0MsUUFBUSxFQUFFLEdBQUc7Z0JBQ2IsWUFBWSxFQUFFLFNBQVM7YUFDdkI7U0FDRCxDQUFDO1FBRUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLGdCQUFnQixDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVLLHlDQUFjLEdBQXBCOzs7Ozs7d0JBQ0MsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO3dCQUNsQixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQzt3QkFDZCxJQUFJLEdBQUc7NEJBQ1YsT0FBTyxFQUFFLENBQUM7NEJBQ1YsV0FBVyxFQUFFLEdBQUc7eUJBQ2hCLENBQUM7Ozs7d0JBR2MscUJBQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxFQUFBOzt3QkFBaEUsUUFBUSxHQUFHLFNBQXFEO3dCQUNwRSxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUM7NEJBQzdCLElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQTt3QkFDNUIsQ0FBQzt3QkFBQyxJQUFJLENBQUMsQ0FBQzs0QkFDUCxJQUFJLENBQUMsT0FBTyxHQUFHLDRCQUE0QixDQUFDO3dCQUM3QyxDQUFDOzs7O3dCQUdELE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBRyxDQUFDLENBQUM7Ozt3QkFFbEIsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDOzs7OztLQUNsQjtJQUVELHFDQUFVLEdBQVY7UUFDQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUM1QixDQUFDO0lBbERXLGdCQUFnQjtRQVA1QixnQkFBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLFNBQVM7WUFDbkIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSwwQkFBMEI7WUFDdkMsU0FBUyxFQUFFLENBQUMsc0JBQXNCLENBQUM7U0FDbkMsQ0FBQzt5Q0FRc0IsNEJBQVk7WUFDZix5QkFBZ0I7T0FQeEIsZ0JBQWdCLENBbUQ1QjtJQUFELHVCQUFDO0NBQUEsQUFuREQsSUFtREM7QUFuRFksNENBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFBhZ2UgfSBmcm9tICd1aS9wYWdlJztcbmltcG9ydCB7IE9yZGVyU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL29yZGVyLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBOYXZpZ2F0aW9uRXh0cmFzIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcblxuQENvbXBvbmVudCh7XG5cdHNlbGVjdG9yOiAncGVuZGluZycsXG5cdG1vZHVsZUlkOiBtb2R1bGUuaWQsXG5cdHRlbXBsYXRlVXJsOiAnLi9wZW5kaW5nLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vcGVuZGluZy5jb21tb24uY3NzJ11cbn0pXG5cbmV4cG9ydCBjbGFzcyBQZW5kaW5nQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblx0bWVzc2FnZTogc3RyaW5nID0gJyc7XG5cdHNvdXJjZTogQXJyYXk8YW55PiA9IFtdO1xuXHRpc0J1c3k6IGJvb2xlYW4gPSBmYWxzZTtcblxuXHRjb25zdHJ1Y3Rvcihcblx0XHRwcml2YXRlIG9yZGVyU2VydmljZTogT3JkZXJTZXJ2aWNlLFxuXHRcdHByaXZhdGUgcm91dGVyRXh0OiBSb3V0ZXJFeHRlbnNpb25zXG5cdCkgeyB9XG5cblx0YXN5bmMgbmdPbkluaXQoKSB7XG5cdFx0YXdhaXQgdGhpcy5nZXRBZ2VudE9yZGVycygpO1xuXHR9XG5cblx0b25JdGVtVGFwKGRhdGEpIHtcblx0XHRsZXQgbmF2aWdhdGlvbkV4dHJhczogTmF2aWdhdGlvbkV4dHJhcyA9IHtcblx0XHRcdHF1ZXJ5UGFyYW1zOiB7XG5cdFx0XHRcdFwiZGF0YVwiOiBKU09OLnN0cmluZ2lmeSh0aGlzLnNvdXJjZVtkYXRhLmluZGV4XSksXG5cdFx0XHRcdFwic3RhdHVzXCI6IFwiUFwiLFxuXHRcdFx0XHRcInN0YXR1c1RleHRcIjogXCJQZW5kaW5nXCJcblx0XHRcdH1cblx0XHR9O1xuXG5cdFx0dGhpcy5yb3V0ZXJFeHQubmF2aWdhdGUoWycvb3JkZXItZGV0YWlscyddLCBuYXZpZ2F0aW9uRXh0cmFzKTtcblx0fVxuXG5cdGFzeW5jIGdldEFnZW50T3JkZXJzKCkge1xuXHRcdHRoaXMuYnVzeVRvZ2dsZSgpO1xuXHRcdHRoaXMubWVzc2FnZSA9ICcnO1xuXHRcdGxldCBib2R5ID0ge1xuXHRcdFx0YWdlbnRJZDogMSxcblx0XHRcdG9yZGVyU3RhdHVzOiAnUCdcblx0XHR9O1xuXG5cdFx0dHJ5IHtcblx0XHRcdGxldCByZXNwb25zZSA9IGF3YWl0IHRoaXMub3JkZXJTZXJ2aWNlLnVzZXJBdXRoKGJvZHksICdhZ2VudE9yZGVycycpO1xuXHRcdFx0aWYgKHJlc3BvbnNlLnN0YXR1cyA9PT0gMjAwKSB7XG5cdFx0XHRcdHRoaXMuc291cmNlID0gcmVzcG9uc2UuZGF0YVxuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0dGhpcy5tZXNzYWdlID0gJ1lvdSBoYXZlIG5vIHBlbmRpbmcgb3JkZXJzJztcblx0XHRcdH1cblx0XHR9XG5cdFx0Y2F0Y2ggKGVycikge1xuXHRcdFx0Y29uc29sZS5sb2coZXJyKTtcblx0XHR9XG5cdFx0dGhpcy5idXN5VG9nZ2xlKCk7XG5cdH1cblxuXHRidXN5VG9nZ2xlKCkge1xuXHRcdHRoaXMuaXNCdXN5ID0gIXRoaXMuaXNCdXN5O1xuXHR9XG59Il19