"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var LoginServiceService = /** @class */ (function () {
    function LoginServiceService(http) {
        this.http = http;
        this.url = 'http://192.168.1.3:3000/api/userAuth';
    }
    LoginServiceService.prototype.userAuth = function (username, password) {
        return __awaiter(this, void 0, void 0, function () {
            var header, body, response, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        header = new http_1.Headers();
                        header.append('Content-Type', 'application/json');
                        body = {
                            username: username,
                            password: password
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.http.post(this.url, JSON.stringify(body), { headers: header }).toPromise()];
                    case 2:
                        response = _a.sent();
                        return [2 /*return*/, response.json()];
                    case 3:
                        err_1 = _a.sent();
                        console.dir(err_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    LoginServiceService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], LoginServiceService);
    return LoginServiceService;
}());
exports.LoginServiceService = LoginServiceService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tc2VydmljZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4tc2VydmljZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBQzNDLHNDQUE4QztBQUM5Qyx1Q0FBcUM7QUFHckM7SUFHSSw2QkFBb0IsSUFBVTtRQUFWLFNBQUksR0FBSixJQUFJLENBQU07UUFGOUIsUUFBRyxHQUFXLHNDQUFzQyxDQUFDO0lBRW5CLENBQUM7SUFFN0Isc0NBQVEsR0FBZCxVQUFlLFFBQWdCLEVBQUUsUUFBZ0I7Ozs7Ozt3QkFDekMsTUFBTSxHQUFHLElBQUksY0FBTyxFQUFFLENBQUM7d0JBQzNCLE1BQU0sQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLGtCQUFrQixDQUFDLENBQUM7d0JBRTVDLElBQUksR0FBRzs0QkFDVCxRQUFRLEVBQUUsUUFBUTs0QkFDbEIsUUFBUSxFQUFFLFFBQVE7eUJBQ3JCLENBQUM7Ozs7d0JBR2lCLHFCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRSxFQUFBOzt3QkFBOUYsUUFBUSxHQUFHLFNBQW1GO3dCQUNsRyxzQkFBTyxRQUFRLENBQUMsSUFBSSxFQUFFLEVBQUM7Ozt3QkFHdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFHLENBQUMsQ0FBQzs7Ozs7O0tBRXhCO0lBckJRLG1CQUFtQjtRQUQvQixpQkFBVSxFQUFFO3lDQUlpQixXQUFJO09BSHJCLG1CQUFtQixDQXVCL0I7SUFBRCwwQkFBQztDQUFBLEFBdkJELElBdUJDO0FBdkJZLGtEQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHAsIEhlYWRlcnMgfSBmcm9tICdAYW5ndWxhci9odHRwJztcbmltcG9ydCAncnhqcy9hZGQvb3BlcmF0b3IvdG9Qcm9taXNlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIExvZ2luU2VydmljZVNlcnZpY2Uge1xuICAgIHVybDogc3RyaW5nID0gJ2h0dHA6Ly8xOTIuMTY4LjEuMzozMDAwL2FwaS91c2VyQXV0aCc7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHApIHsgfVxuXG4gICAgYXN5bmMgdXNlckF1dGgodXNlcm5hbWU6IHN0cmluZywgcGFzc3dvcmQ6IHN0cmluZykge1xuICAgICAgICBsZXQgaGVhZGVyID0gbmV3IEhlYWRlcnMoKTtcbiAgICAgICAgaGVhZGVyLmFwcGVuZCgnQ29udGVudC1UeXBlJywgJ2FwcGxpY2F0aW9uL2pzb24nKTtcblxuICAgICAgICBjb25zdCBib2R5ID0ge1xuICAgICAgICAgICAgdXNlcm5hbWU6IHVzZXJuYW1lLFxuICAgICAgICAgICAgcGFzc3dvcmQ6IHBhc3N3b3JkXG4gICAgICAgIH07XG5cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGxldCByZXNwb25zZSA9IGF3YWl0IHRoaXMuaHR0cC5wb3N0KHRoaXMudXJsLCBKU09OLnN0cmluZ2lmeShib2R5KSwge2hlYWRlcnM6IGhlYWRlcn0pLnRvUHJvbWlzZSgpO1xuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmpzb24oKTtcbiAgICAgICAgfVxuICAgICAgICBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICBjb25zb2xlLmRpcihlcnIpO1xuICAgICAgICB9XG4gICAgfVxuXG59Il19