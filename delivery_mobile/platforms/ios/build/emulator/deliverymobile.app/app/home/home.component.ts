import { Component, OnInit } from "@angular/core";
import { Page } from "ui/page";
import * as dialogModule from 'ui/dialogs';
import { RouterExtensions } from 'nativescript-angular/router';
import * as localStorage from 'nativescript-localstorage';

import { LoginServiceService } from '../services/login-service.service';

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ["./home-common.css"]
})

export class HomeComponent implements OnInit {
    isPassword: boolean = true;
    username: string = 'username';
    password: string = 'password';
    isBusy: boolean = false;

    constructor(
        private page: Page,
        private loginService: LoginServiceService,
        private routerExt: RouterExtensions
    ) {}

    ngOnInit(): void {
        this.page.actionBarHidden = true;
    }
    
    async signIn() {
        this.busyToggle();
        if (!localStorage.getItem('url')) {
            alert('API end point is empty. Go to settings to set it');
            this.busyToggle();
            return false;
        }

        try {
            if (this.username !== '' && this.password !== '') {
                let resp = await this.loginService.userAuth(this.username.toLowerCase(), this.password);
                if (resp.status === 200) {
                    this.routerExt.navigate(['/dashboard'], { clearHistory: true });
                } else {
                    dialogModule.alert('Invalid username or password');
                }
            } else {
                dialogModule.alert('Username and Password are required!');
            }
        }
        catch (err) {
            console.log(err);
        }
        this.busyToggle();
    }

    gotoSettings(): void {
        this.routerExt.navigate(['/settings']);
    }

    passToggle() {
        this.isPassword = !this.isPassword;
    }

    busyToggle() {
        this.isBusy = !this.isBusy;
    }
}
