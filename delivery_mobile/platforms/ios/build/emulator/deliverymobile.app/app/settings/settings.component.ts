import { Component, OnInit } from '@angular/core';
import * as localStorage from 'nativescript-localstorage';
import * as dialogModule from 'ui/dialogs';
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
	selector: 'settings',
	moduleId: module.id,
	templateUrl: './settings.component.html',
	styleUrls: ['./settings.component.css']
})

export class SettingsComponent implements OnInit {
	urlString: string = '';

	constructor(private routerExt: RouterExtensions) { }

	ngOnInit() {
		this.urlString = localStorage.getItem('url');
		if (!this.urlString) {
			this.urlString = 'e.g. http://path-to-api:port/api';
		}
	}

	setURL(): void {
		dialogModule.prompt('Enter URL').then((val) => {
			this.urlString = val.text;
			localStorage.setItem('url', val.text);
		})
		.catch(err => console.log(err));
	}

	goBack(): void {
		this.routerExt.back();
	}
}