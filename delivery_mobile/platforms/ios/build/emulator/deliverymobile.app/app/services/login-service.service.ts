import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import * as localStorage from 'nativescript-localstorage';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class LoginServiceService {
    url: string = '';

    constructor(private http: Http) {
        this.url = localStorage.getItem('url');
    }

    async userAuth(username: string, password: string) {
        this.url = localStorage.getItem('url');
        console.log(this.url);
        let header = new Headers();
        header.append('Content-Type', 'application/json');

        const body = {
            username: username,
            password: 'cGFzc3dvcmQ='
        };

        try {
            let response = await this.http.post(`https://deliveryappserver.herokuapp.com/api/userAuth`, JSON.stringify(body), {headers: header}).toPromise();
            return response.json();
        }
        catch (err) {
            console.dir(err);
        }
    }

}